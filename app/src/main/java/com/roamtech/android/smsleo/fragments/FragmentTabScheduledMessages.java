package com.roamtech.android.smsleo.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.http.NetworkConnectionStatus;
import com.roamtech.android.smsleo.http.RestClient;
import com.roamtech.android.smsleo.http.RestClient2;
import com.roamtech.android.smsleo.util.AsyncLoader;
import com.roamtech.android.smsleo.util.BulkSMSConstant;
import com.roamtech.android.smsleo.util.CommonUtils;
import com.roamtech.android.smsleo.adapters.ScheduledmessagesListLazyAdapter;

public class FragmentTabScheduledMessages extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<HashMap<String, String>>>,SwipeRefreshLayout.OnRefreshListener{
    public static final String TAG = "FragmentTabScheduledMessages";
	public ArrayList<HashMap<String, String>> scheduled_list;
	SharedPreferences app_preference;
	SharedPreferences.Editor editor;
    SwipeRefreshLayout mSwipeRefreshLayout;
    AppCompatActivity mActivity;
    RecyclerView messagesRecycler;
	ScheduledmessagesListLazyAdapter adapter;
	boolean loadingMore = false;
    Boolean isVisible;
    CommonUtils commonUtils;
    public static final int LOADER = 1001;
    ProgressBar mProgresBar;
    public static String messageResult;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BulkSMSConstant.signedOut(mActivity);
        app_preference = PreferenceManager.getDefaultSharedPreferences(mActivity);
        editor = app_preference.edit();
        commonUtils = new CommonUtils();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_sentmessages, container, false);
        messagesRecycler = (RecyclerView) view.findViewById(R.id.lst_sent);
        LinearLayoutManager llm = new LinearLayoutManager(mActivity);
        messagesRecycler.setLayoutManager(llm);
		scheduled_list = new ArrayList<HashMap<String, String>>();
        mProgresBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_container);
        mSwipeRefreshLayout.setColorSchemeColors(R.color.blue_dark_theme,R.color.blueback,R.color.mycolor);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisibleToUser) {
            if(NetworkConnectionStatus.isOnline(mActivity)) {
                mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
            }else{
                Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_connection),Toast.LENGTH_LONG).show();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }
    }
 
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRefresh() {
        if(NetworkConnectionStatus.isOnline(mActivity)) {
            mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
        }else{
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_connection),Toast.LENGTH_LONG).show();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public static class GetScheduledMessages extends AsyncLoader<ArrayList<HashMap<String, String>>> {
        Context ctx;
        public ArrayList<HashMap<String, String>> scheduled_list = new ArrayList<HashMap<String, String>>();
        HashMap<String,String> hashMap = new HashMap<String,String>();
        String app_preference;
        JSONObject jsonMessage = null;

        public GetScheduledMessages(Context context,String app_preference) {
            super(context);
            // TODO Auto-generated constructor stub
            this.ctx = context;
            this.app_preference = app_preference;
        }

        @Override
        public ArrayList<HashMap<String, String>> loadInBackground() {
            Log.d(TAG, "  loader launched ");
            String response = null;
            try {
                hashMap.put(BulkSMSConstant.REQUEST, BulkSMSConstant.SCHEDULE);
                hashMap.put(BulkSMSConstant.CLIENT_ID, app_preference);
                response = RestClient2.makeRestRequest(RestClient2.GET, BulkSMSConstant.SITE_URL2, hashMap);
                Log.d(TAG, "----------------- "+response);
                if(response != null) {
                    jsonMessage = BulkSMSConstant.parseJson(response);
                    if(jsonMessage != null){
                        scheduled_list = parseJson(jsonMessage);
                    }else{
                        scheduled_list = null;
                    }
                }else{
                    scheduled_list = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            return scheduled_list;
        }

        public ArrayList<HashMap<String, String>> parseJson(JSONObject response){
            ArrayList<HashMap<String, String>> scheduled_list = new ArrayList<HashMap<String, String>>();
            String message = null;
            try {
                    if (response.has(BulkSMSConstant.ALERTS) & !response.isNull(BulkSMSConstant.ALERTS)) {
                        JSONArray jsonSchedule = new JSONArray(response.getString(BulkSMSConstant.ALERTS));
                        Log.d(TAG,"---json object--- " + jsonSchedule.toString());
                        JSONObject c;
                        for (int i = 0; i < jsonSchedule.length(); i++) {
                            c = jsonSchedule.getJSONObject(i);
                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put(BulkSMSConstant.ALERTS_ID, c.getString(BulkSMSConstant.ALERTS_ID));
                            map.put(BulkSMSConstant.MESSAGE, c.getString(BulkSMSConstant.MESSAGE));
                            map.put(BulkSMSConstant.CREATED_BY, c.getString(BulkSMSConstant.CREATED_BY));
                            map.put(BulkSMSConstant.SERVICE_ID, c.getString(BulkSMSConstant.SERVICE_ID));
                            map.put(BulkSMSConstant.SEND_TIME, c.getString(BulkSMSConstant.SEND_TIME));
                            map.put(BulkSMSConstant.APPROVED, c.getString(BulkSMSConstant.APPROVED));
                            scheduled_list.add(map);
                        }

                    }else if(response.has(BulkSMSConstant.MESSAGE) & !response.isNull(BulkSMSConstant.MESSAGE)) {
                        message = response.getString(BulkSMSConstant.MESSAGE);
                        messageResult = message;
                        scheduled_list = null;
                    }
                else{
                    scheduled_list = null;
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return scheduled_list;
        }
    }

    @Override
    public Loader<ArrayList<HashMap<String, String>>> onCreateLoader(int i, Bundle bundle) {
        return new GetScheduledMessages(mActivity,app_preference.getString(mActivity.getResources().getString(R.string.bundle_customer_account),""));
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<HashMap<String, String>>> arrayListLoader, ArrayList<HashMap<String, String>> hashMaps) {
        scheduled_list = hashMaps;
        if(scheduled_list != null) {
            adapter = new ScheduledmessagesListLazyAdapter(mActivity,
                    scheduled_list,this);
            messagesRecycler.setAdapter(adapter);
        }else if(messageResult != null && scheduled_list == null){
            Toast.makeText(mActivity,messageResult,Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_error_retrieving),Toast.LENGTH_LONG).show();
        }
        mProgresBar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<HashMap<String, String>>> arrayListLoader) {

    }
}
