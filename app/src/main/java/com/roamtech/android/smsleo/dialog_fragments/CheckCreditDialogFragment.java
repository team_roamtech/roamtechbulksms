package com.roamtech.android.smsleo.dialog_fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.async_tasks.SendCreditBalanceTask;
import com.roamtech.android.smsleo.http.NetworkConnectionStatus;
import com.roamtech.android.smsleo.interfaces.ReturnResult;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by dennis on 8/18/15.
 */
public class CheckCreditDialogFragment extends DialogFragment implements ReturnResult, View.OnClickListener {
    AppCompatActivity mActivity;
    Button btnOk;
    EditText txtMessage;
    SharedPreferences app_preference;
    SharedPreferences.Editor editor;
    HashMap<String, String> valuePairs = new HashMap<String, String>();
    String strMessage,strClientID,balance;
    ProgressBar progressBar;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app_preference = PreferenceManager.getDefaultSharedPreferences(mActivity);
        editor = app_preference.edit();
        strClientID = app_preference.getString(mActivity.getResources().getString(R.string.bundle_customer_account), null);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.dialog_fragment_checkcredit);
        dialog.show();
        dialog.setTitle(mActivity.getResources().getString(R.string.txt_balance_title));
        dialog.setCanceledOnTouchOutside(false);
        progressBar = (ProgressBar) dialog.findViewById(R.id.progressBar);
        txtMessage = (EditText) dialog.findViewById(R.id.txt_check_credit);
        btnOk = (Button) dialog.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(this);
        new SendCreditBalanceTask(mActivity, CheckCreditDialogFragment.this, BulkSMSConstant.FLAG_CREDIT_BALANCE).execute(prepareValuePairs2());

        return dialog;

    }

    @Override
    public void onStartTask() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void onReturnResult(Object result) {
        String strResult = (String) result;
        processCredits(strResult);
    }

    public HashMap<String, String> prepareValuePairs2(){
        valuePairs.put(BulkSMSConstant.CLIENT_ID,strClientID);
        valuePairs.put(BulkSMSConstant.REQUEST, BulkSMSConstant.CREDIT);
        return valuePairs;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_ok:
                dismiss();
                break;
        }
    }

    public void processCredits(String result){
        JSONObject jsonMessage = null;
        if(result != null) {
            jsonMessage = BulkSMSConstant.parseJson(result);
            if(jsonMessage != null){
                processJSONResult(jsonMessage);
            }else{
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_error_retrieving), Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_error_retrieving), Toast.LENGTH_LONG).show();
        }
        progressBar.setVisibility(View.GONE);
    }

    public void processJSONResult(JSONObject response){
        JSONObject jsonResult = null;
        try {
            if (response.has(BulkSMSConstant.CREDIT) & !response.isNull(BulkSMSConstant.CREDIT)) {
                jsonResult = response.getJSONObject(BulkSMSConstant.CREDIT);
                if(jsonResult.has(BulkSMSConstant.BALANCE) & !jsonResult.isNull(BulkSMSConstant.BALANCE)) {
                    balance = jsonResult.getString(BulkSMSConstant.BALANCE);
                    strMessage = mActivity.getResources().getString(R.string.txt_balance);
                    txtMessage.setText(strMessage +" "+ balance);
                }
            }else if(response.has(BulkSMSConstant.MESSAGE) & !response.isNull(BulkSMSConstant.MESSAGE)) {
                strMessage =  response.getString(BulkSMSConstant.MESSAGE);
                txtMessage.setText(strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
