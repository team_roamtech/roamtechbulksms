package com.roamtech.android.smsleo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dennis on 8/13/15.
 */
public class SpinnerSenderAdapter extends ArrayAdapter<ArrayList<HashMap<String, String>>>{
    public static final String TAG = "SpinnerGroupAdapter";
    ArrayList<HashMap<String, String>> sender_list;
    private LayoutInflater vi;
    Context context;


    public SpinnerSenderAdapter(Context context, int resource, ArrayList<HashMap<String, String>> objects) {
        super(context, resource);
        this.sender_list = objects;
        this.context = context;
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        // Log.d(TAG, "groups list size = " + groups_list.size());
        if(sender_list == null){
            return 0;
        }else {
            return sender_list.size();
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View v = vi.inflate(R.layout.spinner_group_items, null);
        TextView txtTitle = (TextView)v.findViewById(R.id.txt_groups);
        HashMap<String, String> data = new HashMap<String, String>();
        data = sender_list.get(position);
        txtTitle.setText(data.get(BulkSMSConstant.SENDER));
        return v;
    }
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        View v = vi.inflate(R.layout.spinner_group_drop_items, null);
        TextView txtTitle = (TextView)v.findViewById(R.id.txt_groups);
        HashMap<String, String> data = new HashMap<String, String>();
        data = sender_list.get(position);
        txtTitle.setText(data.get(BulkSMSConstant.SENDER));
        return v;
    }

}
