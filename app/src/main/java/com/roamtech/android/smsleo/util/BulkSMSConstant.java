package com.roamtech.android.smsleo.util;

import com.roamtech.android.smsleo.LoginActivity;
import com.roamtech.android.smsleo.models.UnitItems;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class BulkSMSConstant {
    public static final String TAG = "BulkSMSConstant";

    public static final String SUBCOUNT = "subcount";
    public static final String SERVICE_ID = "service_id";
    public static final String SERVICE_NAME = "service_name";
    public static final String ALERTS_ID = "alerts_id";
    public static final String ALERTS_DELIVERED_COUNT = "alerts_delivered_count";
    public static final String TOTAL_SENT = "total_sent";
    public static final String MESSAGE = "message";
    public static final String CREATED_BY = "created_by";
    public static final String SEND_TIME = "sendtime";
    public static final String TIME_SENT = "time_sent";
    public static final String APPROVED = "approved";
    public static final String APPROVED_BY = "approved_by";
    public static final String RESPONSE = "response";
    public static final String RESULT = "result";
    public static final String ERROR = "error";
    public static final String INFO = "info";
    public static final String ACCOUNTNAME = "accountname";
    public static final String CLIENT_ID = "client";
    public static final String PHONE = "phone";
    public static final String MSISDN = "msisdn";
    public static final String SUBSCRIPTION_DATE = "subscription_date";
    public static final String GROUPS = "groups";
    public static final String CODE = "code";
    public static final String SENDER = "sender";
    public static final String TEXT = "text";
    public static final String CUSTOMER_ACCOUNT = "customer_account";
    public static final String AMOUNT = "amount";
    public static final String BALANCE = "balance";
    public static final String CURRENCY = "currency";
    public static final String DEBIT = "debit";
    public static final String REMARKS = "remarks";
    public static final String STATUS = "status";
    public static final String TRANSACTION_DATE_TIME = "transaction_date_time";
    public static final String TRANSACTION_TYPE =  "transaction_type";
    public static final String GROUPNAME =  "groupname";
    public static final String RETURN_CODE =  "return_code";
    public static final String DESCRIPTION =  "description";
    public static final String MERCHANT_TRX_ID =  "merchant_trx_id";
    public static final String DEDUCT =  "deduct";
    public static final String CON2GROUP =  "con2group";
    public static final String NUMBER =  "number";
    public static final String CONTACT =  "contact";
    public static final String RATE =  "rate";
    public static final String UNITS =  "units";
    public static final String TYPE =  "type";
    public static final String GROUP_LIST = "group_list";
    public static final String SENDERS_LIST = "senders_list";

    public static final String CMPESA =  "MPESA";
    public static final String KES = "KES";


    public static final int DIALOG_FRAGMENT_SET_DATE = 2001;
    public static final int DIALOG_FRAGMENT_ADD_CONTACTS = 2002;
    public static final int FLAG_ADD_CONTACTS = 1;
    public static final int FLAG_SEND_MESSAGE = 0;
    public static final int FLAG_QUICK_SMS = 2;
    public static final int FLAG_REGISTRATION = 3;
    public static final int FLAG_ADD_GROUP = 4;
    public static final int FLAG_CREDIT_BALANCE = 5;
    public static final int FLAG_MPESA = 6;
    public static final int FLAG_UPDATE_CUSTOMER_CREDIT = 7;
    public static final int FLAG_BILL_CUSTOMER = 9;



    public static final String MPESA = "mpesa";
    public static final String GROUP = "group";
    public static final String GROUPID = "group_id";
    public static final String SCHEDULE = "schedule";
    public static final String SENT = "sent";
    public static final String REQUEST = "request";
    public static final String ALERT = "alert";
    public static final String ALERTS = "alerts";
    public static final String CON = "con";
    public static final String QUICKSMS = "quicksms";
    public static final String EMAIL = "email";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String SIGNUP = "signup";
    public static final String USER = "user";
    public static final String ID = "id";
    public static final String CONTACTS = "contacts";
    public static final String BILLING = "billing";
    public static final String CREDIT = "credit";
    public static final String CALLBACKURL = "http://smsleo.com/index.php/MpesaNotify";
    public static final String CALLBACK = "callback";
    public static final String SUCCESS = "success";

	public final static String SITE_URL = "http://sms.roamtech.com/android_bulk_api/index.php";
    //public static final String LIPANAMPESAURL="http://pay.smsleo.com/?request=mpesa";
    public static final String LIPANAMPESAURL="http://197.248.6.28/pay/?request=mpesa";
    //public static final String LIPANAMPESAURL="http://197.248.6.28/pay/?";
    //public static final String SITE_URL2="http://teamroot.mobi/leoapi";
    public static final String SITE_URL2="http://api.smsleo.com";
    //public static final String SITE_URL2="http://test.smsleo.com";
    //public static final String SITE_URL2="http://192.168.1.4/leoapi/";
    //public static final String USER_DIRECTORY = "192.168.1.172";
    public static final String USER_DIRECTORY ="197.232.18.245";
    public static final String SERVER_DETAILS = "ou=smsleo,dc=roamtech,dc=com";
    public static final String BASEDNS = "dc=roamtech,dc=com";
    public static final String ATTR_CN_NAME = "cn";
    public static final String ATTR_SN_NAME = "sn";
    public static final String ATTR_CLIENT_ID = "clientid";
    public static final String ATTR_EMALI_ROLES = "emaliroles";
    public static final String ATTR_GIVEN_NAME = "givenName";
    public static final String ATTR_PRIMARY_MAIL= "mail";
    public static final String ATTR_UID= "uid";
	static SharedPreferences app_preference;
    static HashMap<UnitItems,Double> rates = new HashMap<UnitItems, Double>();

	public static void signedOut(Context con){
		app_preference = PreferenceManager.getDefaultSharedPreferences(con);
		if(app_preference.getString("clientid", "") == null){
			Intent intent = new Intent(con,LoginActivity.class);
			con.startActivity(intent);
		}
	}

    public static JSONObject parseJson(String response) {
        JSONObject json = null;
        JSONObject jsonResult = null;
        try {
            json = new JSONObject(response);
            if (json.has(BulkSMSConstant.RESULT) & !json.isNull(BulkSMSConstant.RESULT)) {
                jsonResult = json.getJSONObject(BulkSMSConstant.RESULT);
                Log.d(TAG, jsonResult.toString());

            }else if(json.has(BulkSMSConstant.ERROR) & !json.isNull(BulkSMSConstant.ERROR)) {
                jsonResult = json.getJSONObject(BulkSMSConstant.ERROR);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonResult;
    }

    public static double [] checkRate(String strUnits){
        ArrayList<UnitItems> arrUnitItems = createTable();
        int units = Integer.parseInt(strUnits);
        int position = 0;
        for(int i = 0; i < arrUnitItems.size();i++){
            if(units <= arrUnitItems.get(i).getSecondItem() && units >= arrUnitItems.get(i).getFirstItem()){
                position = i;
            }
        }
        double [] cost = {units * rates.get(arrUnitItems.get(position)),rates.get(arrUnitItems.get(position))};
        return cost;
    }

    public static ArrayList<UnitItems> createTable(){
        ArrayList<UnitItems> arrUnitItems = new ArrayList<UnitItems>();
        UnitItems unitItems0 = new UnitItems();
        unitItems0.setFirstItem(0); unitItems0.setSecondItem(100000);
        arrUnitItems.add(unitItems0);
        UnitItems unitItems1 = new UnitItems();
        unitItems1.setFirstItem(100000); unitItems1.setSecondItem(250000);
        arrUnitItems.add(unitItems1);
        UnitItems unitItems2 = new UnitItems();
        unitItems2.setFirstItem(250000); unitItems2.setSecondItem(500000);
        arrUnitItems.add(unitItems2);
        UnitItems unitItems3 = new UnitItems();
        unitItems3.setFirstItem(500000); unitItems3.setSecondItem(2000000);
        arrUnitItems.add(unitItems3);
        UnitItems unitItems4 = new UnitItems();
        unitItems4.setFirstItem(2000000); unitItems4.setSecondItem(5000000);
        arrUnitItems.add(unitItems4);
        rates.put(arrUnitItems.get(0),0.8);
        rates.put(arrUnitItems.get(1),0.7);
        rates.put(arrUnitItems.get(2),0.6);
        rates.put(arrUnitItems.get(3),0.5);
        rates.put(arrUnitItems.get(4),0.4);
        return arrUnitItems;
    }
}
