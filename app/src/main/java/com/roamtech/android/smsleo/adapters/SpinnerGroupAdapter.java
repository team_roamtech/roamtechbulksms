package com.roamtech.android.smsleo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

/**
 * Created by dennis on 6/18/15.
 */
public class SpinnerGroupAdapter extends ArrayAdapter<ArrayList<HashMap<String, String>>> {
    public static final String TAG = "SpinnerGroupAdapter";
    ArrayList<HashMap<String, String>> groups_list;
    private LayoutInflater vi;
    Context context;


    public SpinnerGroupAdapter(Context context, int resource, ArrayList<HashMap<String, String>> objects) {
        super(context, resource);
        this.groups_list = objects;
        this.context = context;
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
       // Log.d(TAG, "groups list size = " + groups_list.size());
        if(groups_list == null){
            return 0;
        }else {
            return groups_list.size();
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View v = vi.inflate(R.layout.spinner_group_items, null);
        TextView txtTitle = (TextView)v.findViewById(R.id.txt_groups);
        HashMap<String, String> data = new HashMap<String, String>();
        data = groups_list.get(position);
        txtTitle.setText(data.get(BulkSMSConstant.SERVICE_NAME));
        return v;
    }
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        View v = vi.inflate(R.layout.spinner_group_drop_items, null);
        TextView txtTitle = (TextView)v.findViewById(R.id.txt_groups);
        HashMap<String, String> data = new HashMap<String, String>();
        data = groups_list.get(position);
        txtTitle.setText(data.get(BulkSMSConstant.SERVICE_NAME));
        return v;
    }

}
