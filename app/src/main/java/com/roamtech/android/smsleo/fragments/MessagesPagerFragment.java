package com.roamtech.android.smsleo.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.adapters.ViewPagerAdapter;
import com.roamtech.android.smsleo.util.AsyncLoader;
import com.roamtech.android.smsleo.views.SlidingTabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessagesPagerFragment extends Fragment implements LoaderManager.LoaderCallbacks<String[]>{
    private SlidingTabLayout mSlidingTabLayout;
    ViewPager mViewPager;
    AppCompatActivity mActivity;
    public static final int TAB_FRAGMENT = 2;
    public static final int LOADER = 1005;


    public MessagesPagerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_messagespager2, container, false);
        mViewPager = (ViewPager) rootView.findViewById(R.id.pager);
        mViewPager.setOffscreenPageLimit(0);
        mSlidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.sliding_tabs);
        //mSlidingTabLayout.setCustomTabView(R.layout.tab_textview,R.id.txt_tab_title);
        mActivity.getSupportLoaderManager().initLoader(LOADER, null, this);
        return rootView;
    }

    public static class GetFragments extends AsyncLoader<String[]> {
        String [] arrayItems;
        Context ctx;

        public GetFragments(Context context,String [] arrayItems) {
            super(context);
            // TODO Auto-generated constructor stub
            this.ctx = context;
            this.arrayItems = arrayItems;

        }

        @Override
        public String[] loadInBackground() {
            return null;
        }
        //Process the JSON result to Data we can use
    }


    @Override
    public Loader<String[]> onCreateLoader(int id, Bundle args) {
        return new GetFragments(mActivity,null);
    }

    @Override
    public void onLoadFinished(Loader<String[]> loader, String[] data) {
        mViewPager.setAdapter(new ViewPagerAdapter(mActivity.getSupportFragmentManager(),mActivity));
        mSlidingTabLayout.setViewPager(mViewPager);
        mViewPager.setCurrentItem(0);

    }

    @Override
    public void onLoaderReset(Loader<String[]> loader) {

    }
}
