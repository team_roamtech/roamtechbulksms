package com.roamtech.android.smsleo.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.async_tasks.SendPayTask;
import com.roamtech.android.smsleo.async_tasks.SendScheduledMessagesTask;
import com.roamtech.android.smsleo.http.NetworkConnectionStatus;
import com.roamtech.android.smsleo.interfaces.ReturnResult;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PayBillFragments extends Fragment implements View.OnClickListener,ReturnResult{
    public static final String TAG = "PayBillFragments";
    ProgressBar mProgressBar;
    EditText editPhoneNum;
    EditText editAmount;
    EditText editUnits;
    Button btnPay;
    AppCompatActivity mActivity;
    String amount;
    String msisdn;
    int flag;
    HashMap<String, String> valuePairs = new HashMap<String, String>();
    ProgressDialog progressDialog;
    String resultCode,description,merchant_trx_id;
    double [] dAmount;
    public static String messageResult;
    String strClientID;
    SharedPreferences app_preference;
    SharedPreferences.Editor editor;

    public PayBillFragments() {
        // Required empty public constructor
    }

    public void onAttach(Context context){
        super.onAttach(context);
        mActivity = (AppCompatActivity) context;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        app_preference = PreferenceManager.getDefaultSharedPreferences(mActivity);
        editor = app_preference.edit();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_pay_bill_fragments, container, false);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        editPhoneNum = (EditText) rootView.findViewById(R.id.edit_phone_num);
        editAmount = (EditText) rootView.findViewById(R.id.edit_amount);
        editUnits = (EditText) rootView.findViewById(R.id.edit_units);
        editUnits.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    if (Double.parseDouble(s.toString()) <= 5000000) {
                        dAmount = BulkSMSConstant.checkRate(s.toString());
                        editAmount.setText(String.valueOf(dAmount[0]));
                    } else {
                        Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_maximum_limit), Toast.LENGTH_LONG).show();
                    }
                } else {
                    editAmount.setText("");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btnPay = (Button) rootView.findViewById(R.id.btn_pay);
        btnPay.setOnClickListener(this);
        return rootView;
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_pay:
                fillData();
                break;
        }
    }

    private void fillData(){
        Bundle args = new Bundle();
        if(editPhoneNum.getText().length() < 1) {
            Toast.makeText(mActivity,"Enter Phone Number",Toast.LENGTH_LONG).show();

        }else if(editAmount.getText().length() < 1){
            Toast.makeText(mActivity,"Enter Amount",Toast.LENGTH_LONG).show();

        }else if(editUnits.getText().length() < 1){
            Toast.makeText(mActivity,"Enter Units",Toast.LENGTH_LONG).show();

        }else if(!isValidPhoneNum(editPhoneNum.getText())){
            Toast.makeText(mActivity,"Enter valid phone number",Toast.LENGTH_LONG).show();
        }else{
            msisdn = editPhoneNum.getText().toString();
            amount = editAmount.getText().toString();

            progressDialog = new ProgressDialog(mActivity);
            progressDialog.setMessage(mActivity.getResources().getString(R.string.dialog_sending));
            progressDialog.show();

            flag = BulkSMSConstant.FLAG_MPESA;
            new SendPayTask(mActivity,this,BulkSMSConstant.FLAG_MPESA).execute(prepareValuePairs());

        }
    }

    public HashMap<String, String> prepareValuePairs(){
        valuePairs.put(BulkSMSConstant.AMOUNT, amount);
        valuePairs.put(BulkSMSConstant.PHONE, msisdn);
        valuePairs.put(BulkSMSConstant.CALLBACK, BulkSMSConstant.CALLBACKURL);
        return valuePairs;
    }

    public HashMap<String, String> prepareValuePairs2(){
        strClientID = app_preference.getString(mActivity.getResources().getString(R.string.bundle_customer_account), null);
        valuePairs.put(BulkSMSConstant.REQUEST, BulkSMSConstant.BILLING);
        valuePairs.put(BulkSMSConstant.CLIENT_ID,strClientID);
        valuePairs.put(BulkSMSConstant.RATE,String.valueOf(dAmount[1]));
        valuePairs.put(BulkSMSConstant.UNITS, editUnits.getText().toString());
        valuePairs.put(BulkSMSConstant.TYPE, BulkSMSConstant.CMPESA);
        valuePairs.put(BulkSMSConstant.AMOUNT, String.valueOf(dAmount[0]));
        valuePairs.put(BulkSMSConstant.CURRENCY, BulkSMSConstant.KES);
        valuePairs.put(BulkSMSConstant.REMARKS, merchant_trx_id);
        return valuePairs;
    }

    public void parseJson(String response){
        JSONObject json = null;
        try {
            json = new JSONObject(response);
            if(json.has(BulkSMSConstant.ERROR) & !json.isNull(BulkSMSConstant.ERROR)) {
                Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_failed),Toast.LENGTH_LONG).show();
            }else {
                if (json.has(BulkSMSConstant.RETURN_CODE) & !json.isNull(BulkSMSConstant.RETURN_CODE)) {
                    resultCode = json.getString(BulkSMSConstant.RETURN_CODE);
                }
                if (json.has(BulkSMSConstant.DESCRIPTION) & !json.isNull(BulkSMSConstant.DESCRIPTION)) {
                    description = json.getString(BulkSMSConstant.DESCRIPTION);
                }
                if (json.has(BulkSMSConstant.MERCHANT_TRX_ID) & !json.isNull(BulkSMSConstant.MERCHANT_TRX_ID)) {
                    merchant_trx_id = json.getString(BulkSMSConstant.MERCHANT_TRX_ID);
                }
                Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_send),Toast.LENGTH_LONG).show();
                flag = BulkSMSConstant.FLAG_BILL_CUSTOMER;
                new SendScheduledMessagesTask(mActivity, PayBillFragments.this, BulkSMSConstant.FLAG_BILL_CUSTOMER).execute(prepareValuePairs2());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Check validity of the phone number
    public final static boolean isValidPhoneNum(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.PHONE.matcher(target).matches();
    }

    @Override
    public void onStartTask() {

    }

    @Override
    public void onReturnResult(Object result) {
        String strResult = (String) result;
        if(strResult != null){
            processPayments(strResult);
        }else{
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_error_retrieving),Toast.LENGTH_LONG).show();
        }
    }

    public void processPayments(String result){
        Log.d(TAG, result);
        JSONObject jsonMessage = null;
        if(result != null) {
            if(flag == BulkSMSConstant.FLAG_MPESA){
                parseJson(result);
            }else if(flag == BulkSMSConstant.FLAG_BILL_CUSTOMER){
                jsonMessage = BulkSMSConstant.parseJson(result);
                if(jsonMessage != null) {
                    processJSONResult(jsonMessage);
                }
            }else{
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_error_retrieving), Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_error_retrieving), Toast.LENGTH_LONG).show();
        }
        progressDialog.dismiss();
    }

    public void processJSONResult(JSONObject response){
        JSONObject jsonResult = null;
        try {
            if(response.has(BulkSMSConstant.BILLING) & !response.isNull(BulkSMSConstant.BILLING)) {
                jsonResult = response.getJSONObject(BulkSMSConstant.BILLING);
                if(jsonResult.has(BulkSMSConstant.BALANCE) & !jsonResult.isNull(BulkSMSConstant.BALANCE)) {
                    messageResult = mActivity.getResources().getString(R.string.txt_balance)+" "+jsonResult.getString(BulkSMSConstant.BALANCE);
                    Toast.makeText(mActivity, messageResult, Toast.LENGTH_LONG).show();
                }
            } else if(response.has(BulkSMSConstant.MESSAGE) & !response.isNull(BulkSMSConstant.MESSAGE)) {
                messageResult =  response.getString(BulkSMSConstant.MESSAGE);
                Toast.makeText(mActivity,messageResult,Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
