package com.roamtech.android.smsleo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.roamtech.android.smsleo.adapters.DrawerAdapter;
import com.roamtech.android.smsleo.dialog_fragments.CheckCreditDialogFragment;
import com.roamtech.android.smsleo.dialog_fragments.ConfirmDialog;
import com.roamtech.android.smsleo.fragments.BillingHistoryFragment;
import com.roamtech.android.smsleo.fragments.ManageContactsFragment;
import com.roamtech.android.smsleo.fragments.MessagesPagerFragment;
import com.roamtech.android.smsleo.fragments.PayBillFragments;
import com.roamtech.android.smsleo.fragments.QuickSMSFragment;
import com.roamtech.android.smsleo.fragments.AddContactGroupFragment;
import com.roamtech.android.smsleo.interfaces.CheckConfirm;
import com.roamtech.android.smsleo.interfaces.LaunchResults;
import com.roamtech.android.smsleo.util.BulkSMSConstant;
import com.roamtech.android.smsleo.util.CommonUtils;

import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

/**
 * Created by dennis on 6/10/15.
 */
public class HomeActivity2 extends AppCompatActivity implements CheckConfirm,LaunchResults {
    public static final String TAG = "HomeActivity2";
    public static final String REQUESTHELP = "request_help";
    private RecyclerView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    String [] drawerListArray;
    Toolbar toolBar;
    TypedArray listItemsImgArray;
    DrawerAdapter drawerAdapter;
    SharedPreferences app_preference;
    SharedPreferences.Editor editor;
    MessagesPagerFragment messagesPagerFragment;
    QuickSMSFragment quickSMSFragment;
    ManageContactsFragment manageContactsFragment;
    PayBillFragments payBillFragments;
    BillingHistoryFragment billingHistoryFragment;
    AddContactGroupFragment addContactGroupFragment;
    ConfirmDialog newDialogFragment7;
    CheckCreditDialogFragment newDialogFragment8;
    Bundle sgnOut = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home2);
        app_preference = PreferenceManager.getDefaultSharedPreferences(this);
        editor = app_preference.edit();
        sgnOut.putString(CommonUtils.SIGNOUT,this.getResources().getString(R.string.txt_advisory_exit));
        sgnOut.putString(CommonUtils.RETURN_TAG,HomeActivity2.TAG);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (RecyclerView) findViewById(R.id.listview_drawer);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        mDrawerList.setLayoutManager(llm);
        drawerListArray = getResources().getStringArray(R.array.home_activity_items2);
        listItemsImgArray = getResources().obtainTypedArray(R.array.home_activity_imgs2);
        toolBar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                toolBar,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
               // toolBar.setNavigationIcon(R.drawable.ic_action_overflow2);
            }
            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
                //toolBar.setNavigationIcon(R.drawable.ic_action_arrow);
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "mdrawer Toggle touched");

                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }

            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        drawerAdapter = new DrawerAdapter(this,drawerListArray,listItemsImgArray);
        mDrawerList.setAdapter(drawerAdapter);
        quickSMSFragment = new QuickSMSFragment();
        manageContactsFragment = new ManageContactsFragment();
        addContactGroupFragment = new AddContactGroupFragment();

        if(savedInstanceState == null) {
            Bundle args = new Bundle();
            messagesPagerFragment = new MessagesPagerFragment();
            messagesPagerFragment.setArguments(args);
            CommonUtils.addFragment(messagesPagerFragment, false, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) messagesPagerFragment).getClass().getName());
        }

    }

    @Override
    public void confirmYes(String tag) {
        if(tag.equals(HomeActivity2.TAG)) {
            editor.clear().commit();
            //Exit the application
            android.os.Process.killProcess(android.os.Process.myPid());
            HomeActivity2.super.onDestroy();
            finish();
        }
    }

    @Override
    public void confirmNo(String tag){
        if(tag.equals(HomeActivity2.TAG)){

        }
    }

    @Override
    public void launchResults(int position, Bundle bundle, int view, View itemView) {
        switch(view){
            case R.id.lyt_drawer_items:
                selectItem(position);
                break;
            default:
                break;
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d(TAG,"position headerflag selected");
            selectItem(position);
        }
    }

    public void selectItem(int position){
        Bundle args = new Bundle();

        switch(position){
            case 0:
                messagesPagerFragment = new MessagesPagerFragment();
                messagesPagerFragment.setArguments(args);
                CommonUtils.addFragment(messagesPagerFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) messagesPagerFragment).getClass().getName());
                break;
            case 1:
                addContactGroupFragment = new AddContactGroupFragment();
                addContactGroupFragment.setArguments(args);
                CommonUtils.addFragment(addContactGroupFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) addContactGroupFragment).getClass().getName());
                break;
            case 2:
                manageContactsFragment = new ManageContactsFragment();
                manageContactsFragment.setArguments(args);
                CommonUtils.addFragment(manageContactsFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) manageContactsFragment).getClass().getName());
                break;
            case 3:
                quickSMSFragment = new QuickSMSFragment();
                quickSMSFragment.setArguments(args);
                CommonUtils.addFragment(quickSMSFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) quickSMSFragment).getClass().getName());

                break;
            case 4:
                payBillFragments = new PayBillFragments();
                payBillFragments.setArguments(args);
                CommonUtils.addFragment(payBillFragments, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) payBillFragments).getClass().getName());
                break;
            case 5:
                billingHistoryFragment = new BillingHistoryFragment();
                billingHistoryFragment.setArguments(args);
                CommonUtils.addFragment(billingHistoryFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) billingHistoryFragment).getClass().getName());
                break;
            case 6:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Confirm");
                builder.setMessage("Do you want to request for some help?");
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestForhelp(HomeActivity2.this);
                    }
                });
                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

                break;
            case 7:

                newDialogFragment7 = new ConfirmDialog();
                newDialogFragment7.setArguments(sgnOut);
                newDialogFragment7.show(CommonUtils.addDialogFragment(getSupportFragmentManager(), CommonUtils.DIALOG), CommonUtils.DIALOG);
                break;
        }
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home2, menu);
        menu.findItem(R.id.action_check_credit).setVisible(true);
        menu.findItem(R.id.action_add_contact).setVisible(false);
        menu.findItem(R.id.action_log_out).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_log_out) {
            newDialogFragment7 = new ConfirmDialog();
            newDialogFragment7.setArguments(sgnOut);
            newDialogFragment7.show(CommonUtils.addDialogFragment(getSupportFragmentManager(), CommonUtils.DIALOG), CommonUtils.DIALOG);
        }else if(id == R.id.action_check_credit){
            newDialogFragment8 = new CheckCreditDialogFragment();
            newDialogFragment8.show(CommonUtils.addDialogFragment(getSupportFragmentManager(), CommonUtils.DIALOG), CommonUtils.DIALOG);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    //Send for help
    public void requestForhelp(Activity mActivity) {
        try {
            AsyncHttpClient client = new AsyncHttpClient();
            final ProgressDialog progressDialog = new ProgressDialog(mActivity);
            progressDialog.setButton(ProgressDialog.BUTTON_NEGATIVE, getString(R.string.okay), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            JSONObject jsonParams2 = new JSONObject();
            jsonParams2.put("code", app_preference.getString("code", ""));// String
            // client_id
            // =
            // app_preference.getString("clientid",
            // "");"705702"
            jsonParams2.put("service_id", app_preference.getString("serviceid", ""));// "6013852000041527"
            jsonParams2.put("msisdn", ": " + app_preference.getString("givenName", "") + "-" + app_preference.getString("client_name", ""));

            JSONObject jsonParams = new JSONObject();
            jsonParams.put("tag", "HELP");
            jsonParams.put("query", jsonParams2);// phone number,
            // service_id,
            System.out.println(">>JSON: " + jsonParams);
            // code
            StringEntity entity = new StringEntity(jsonParams.toString());

            client.post(mActivity, BulkSMSConstant.SITE_URL, entity, "application/json", new AsyncHttpResponseHandler() {
                @Override
                public void onStart() {
                    progressDialog.setMessage("Requesting help...");
                    progressDialog.show();
                }

                @Override
                public void onSuccess(String resp) {
                    try {
                        System.out.println(">>XXX: " + resp);
                        progressDialog.setTitle("ROAMTECH");
                        progressDialog.setMessage("You will receive a call from our customer care representative shortly.");
                    } catch (Exception e) {
                        progressDialog.setMessage("There seems to be a problem requesting help.");
                    }
                }

                @Override
                public void onFailure(Throwable e, String response) {
                    System.out.println(">>XXXE: " + response);
                    progressDialog.setMessage("There was a problem with system response. Please try again or notify support.");
                }

                @Override
                public void onFinish() {
                    progressDialog.setCancelable(true);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setIndeterminateDrawable(null);
                    final Button cancelButton = progressDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                    cancelButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            progressDialog.dismiss();
                        }
                    });
                }
            });
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

}
