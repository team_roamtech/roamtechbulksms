package com.roamtech.android.smsleo.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.fragments.FragmentTabScheduledMessages;
import com.roamtech.android.smsleo.fragments.FragmentTabSendMessage;
import com.roamtech.android.smsleo.fragments.FragmentTabSentMessages;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

	// Declare the number of ViewPager pages
	final int PAGE_COUNT = 3;
    Context ctx;

	public ViewPagerAdapter(FragmentManager fm,Context ctx) {
		super(fm);
        this.ctx = ctx;
	}

	@Override
	public Fragment getItem(int arg0) {
        if(arg0 == 0) {
            FragmentTabSendMessage fragmentSendMessage = new FragmentTabSendMessage();
            return fragmentSendMessage;
        }else if(arg0 == 1) {
            FragmentTabScheduledMessages fragmentScheduledMessages = new FragmentTabScheduledMessages();
            return fragmentScheduledMessages;
        }else if(arg0 == 2) {
            FragmentTabSentMessages fragmentTabSentMessages = new FragmentTabSentMessages();
            return fragmentTabSentMessages;
        }
        return null;
	}

    // BEGIN_INCLUDE (pageradapter_getpagetitle)
    /**
     * Return the title of the item at {@code position}. This is important as what this method
     * returns is what is displayed in the {@link com.roamtech.android.smsleo.views.SlidingTabLayout}.
     * <p>
     * Here we construct one using the position value, but for real application the title should
     * refer to the item's contents.
     */
    @Override
    public CharSequence getPageTitle(int position) {
        switch(position){
            case 0:
                return ctx.getResources().getString(R.string.send);
            case 1:
                return ctx.getResources().getString(R.string.scheduled);
            case 2:
                return ctx.getResources().getString(R.string.sent);
        }
        return null;

    }

	@Override
	public int getCount() {
		return PAGE_COUNT;
	}

    @Override
    public Parcelable saveState(){
        return null;
    }

}