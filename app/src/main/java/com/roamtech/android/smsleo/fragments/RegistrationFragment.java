package com.roamtech.android.smsleo.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.async_tasks.SendScheduledMessagesTask;
import com.roamtech.android.smsleo.http.NetworkConnectionStatus;
import com.roamtech.android.smsleo.interfaces.GoToPager;
import com.roamtech.android.smsleo.interfaces.ReturnResult;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationFragment extends Fragment implements View.OnClickListener,ReturnResult {
    AppCompatActivity mActivity;
    public static final String TAG = "RegistrationFragment";
    EditText username, password, email, accountName;
    Button btnLogin;
    String strUsername,strPassword,strEmail,strClientID, strAccountName;
    SharedPreferences app_preference;
    SharedPreferences.Editor editor;
    ProgressDialog progressDialog;
    HashMap<String, String> valuePairs = new HashMap<String, String>();
    Boolean isVisible;
    GoToPager mCallBack2;


    public RegistrationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            mCallBack2 = (GoToPager)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_registration, container, false);
        btnLogin = (Button) rootView.findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        username = (EditText) rootView.findViewById(R.id.eTextUsername);
        password = (EditText) rootView.findViewById(R.id.eTextPassword);
        email = (EditText) rootView.findViewById(R.id.eTextEmail);
        accountName = (EditText) rootView.findViewById(R.id.eTextaccountname);
        app_preference = PreferenceManager.getDefaultSharedPreferences(mActivity);
        editor = app_preference.edit();
        return rootView;
    }

    public void fillData(){
        if(username.getText().length() < 1 || password.getText().length() < 1 || email.getText().toString().length() < 1 ||accountName.getText().length() < 1){
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_enter_details), Toast.LENGTH_LONG).show();
        }else{
            strUsername = username.getText().toString();
            strPassword = password.getText().toString();
            strEmail = email.getText().toString();
            strAccountName = accountName.getText().toString();
            if(NetworkConnectionStatus.isOnline(mActivity)) {
                new SendScheduledMessagesTask(mActivity, RegistrationFragment.this, BulkSMSConstant.FLAG_REGISTRATION).execute(prepareValuePairs());
            }else{
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_no_connection), Toast.LENGTH_LONG).show();
            }
        }
    }

    public HashMap<String, String> prepareValuePairs(){
        strClientID = app_preference.getString(mActivity.getResources().getString(R.string.bundle_customer_account), null);
        valuePairs.put(BulkSMSConstant.CLIENT_ID,strClientID);
        valuePairs.put(BulkSMSConstant.ACCOUNTNAME,strAccountName);
        valuePairs.put(BulkSMSConstant.EMAIL,strEmail);
        valuePairs.put(BulkSMSConstant.USERNAME,strUsername);
        valuePairs.put(BulkSMSConstant.PASSWORD,strPassword);
        valuePairs.put(BulkSMSConstant.REQUEST,BulkSMSConstant.SIGNUP);
        return valuePairs;
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnLogin:
                fillData();
                break;
        }
    }

    private String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is;

            is = mActivity.getAssets().open("customer_account.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    @Override
    public void onStartTask() {
        progressDialog = new ProgressDialog(mActivity);
        progressDialog.setMessage(mActivity.getResources().getString(R.string.dialog_sign_up));
        progressDialog.show();
    }

    @Override
    public void onReturnResult(Object result) {
        String strResult = (String) result;
        JSONObject message = null;
        //strResult = loadJSONFromAsset();
        String strMessage = null;
        if(strResult != null){
            Log.d(TAG,strResult);
            message = BulkSMSConstant.parseJson(strResult);
            if(message != null) {
                parseJson(message);
            }else {
                Log.d(TAG, "parseJson error null message");
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_advisory_invalid_details2), Toast.LENGTH_LONG).show();
            }
        }else{
            Log.d(TAG, "parseJson error no Asset");
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_advisory_invalid_details2), Toast.LENGTH_LONG).show();
        }
        Log.d(TAG, "----------- "+strResult);
        progressDialog.dismiss();
    }

    public void parseJson(JSONObject response) {
        JSONObject jsonResult = null;
        String message = null;
        try {
            if (response.has(BulkSMSConstant.USER) & !response.isNull(BulkSMSConstant.USER)) {
                jsonResult = response.getJSONObject(BulkSMSConstant.USER);
                if(jsonResult.has(BulkSMSConstant.INFO) & !jsonResult.isNull(BulkSMSConstant.INFO)) {
                    message = jsonResult.getString(BulkSMSConstant.INFO);
                    Log.d(TAG, "parseJson"+ message);
                    Toast.makeText(mActivity, message, Toast.LENGTH_LONG).show();
                    mCallBack2.moveToPager(0, 0);
                    //Scroll back to Login
                }else{
                    Log.d(TAG, "parseJson error");
                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_advisory_invalid_details2), Toast.LENGTH_LONG).show();
                    message = null;
                }
            }else if(response.has(BulkSMSConstant.MESSAGE) & !response.isNull(BulkSMSConstant.MESSAGE)){
                    message = response.getString(BulkSMSConstant.MESSAGE);
                    Toast.makeText(mActivity, message, Toast.LENGTH_LONG).show();
            }else {
                Log.d(TAG, "parseJson ");
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_advisory_invalid_details2), Toast.LENGTH_LONG).show();
                message = null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
}
}
