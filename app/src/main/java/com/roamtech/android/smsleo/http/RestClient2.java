package com.roamtech.android.smsleo.http;

import android.content.ContentValues;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dennis on 2/2/16.
 */
public class RestClient2 {

    public static final String TAG = "RestClient";
    static String data;
    public static final int GET = 0;
    public static final int POST = 1;
    InputStream is = null;

    public static String makeMpesaRequest(int method,String myUrl,String json)throws Exception{
        InputStream is = null;
        Log.d(TAG,"URL IS "+myUrl);
        URL url = new URL(myUrl);

        if (method == POST) {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            try {
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                //Writing dataToSend to outputstreamwriter
                writer.write(json);

                //Sending the data to the server - This much is enough to send data to server
                //But to read the response of the server, you will have to implement the procedure below
                writer.flush();

                int response = conn.getResponseCode();
                // String responseMsg = (String) conn.getContent();
                Log.d(TAG, "The response is: " + response);

                //Data Read Procedure - Basically reading the data comming line by line
                is = conn.getInputStream();
                data = readIt(is);
                Log.d(TAG, data);
            } catch (IOException e) {
                is = conn.getErrorStream();
                data = readIt(is);
                Log.d(TAG, data);
                e.printStackTrace();
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        }
        return data;
    }

    public static String makeRestRequest(int method, String myUrl, HashMap<String,String> params) throws Exception{
        String encodedStr = null;
        if(params != null) {
            encodedStr = getEncodedData(params);
        }
        InputStream is = null;
        Log.d(TAG,"URL IS "+myUrl);
        URL url = new URL(myUrl);

        if (method == POST){
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //final String basicAuth = "Basic " + Base64.encodeToString("testuser:testuser".getBytes(), Base64.NO_WRAP);
            //conn.setRequestProperty("Authorization", basicAuth);
            Log.d(TAG,"POST Method");
            try {
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                //Writing dataToSend to outputstreamwriter
                writer.write(encodedStr);

                //Sending the data to the server - This much is enough to send data to server
                //But to read the response of the server, you will have to implement the procedure below
                writer.flush();

                int response = conn.getResponseCode();
                // String responseMsg = (String) conn.getContent();
                Log.d(TAG, "The response is: " + response );

                //Data Read Procedure - Basically reading the data comming line by line
                is = conn.getInputStream();
                data = readIt(is);
                Log.d(TAG,data);
            }catch (IOException e){
                is = conn.getErrorStream();
                data = readIt(is);
                Log.d(TAG,data);
                e.printStackTrace();
            }finally {
                if (is != null) {
                    is.close();
                }
            }

        }else if(method == GET){
            url = new URL(myUrl +"?"+getEncodedData(params));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
          //  final String basicAuth = "Basic " + Base64.encodeToString("testuser:testuser".getBytes(), Base64.NO_WRAP);
            //conn.setRequestProperty("Authorization", basicAuth);
            Log.d(TAG,"Get Method");
            try {
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int response = conn.getResponseCode();
                // String responseMsg = (String) conn.getErrorStream();
                Log.d(TAG, "The response is: " + response);

                is = conn.getInputStream();

                data = readIt(is);
                Log.d(TAG,data);
            }catch (IOException e){
                is = conn.getErrorStream();
                data = readIt(is);
                Log.d(TAG,data);
                e.printStackTrace();
            }finally {
                if (is != null) {
                    is.close();
                }
            }
        }
        return data;
    }
    public static void sendClaimImages(){

    }




    public static String readIt(InputStream is) {
        StringBuilder sb = null;
        if (is != null) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
                sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                is.close();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }
        return "error:";
    }
   /* public static String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Log.d(TAG,"Input Stream "+stream);
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");

        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }*/

    public static String getEncodedData(HashMap<String,String> data) {
        StringBuilder sb = new StringBuilder();
        for(String key : data.keySet()) {
            String value = null;
            try {
                value = URLEncoder.encode(data.get(key), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if(sb.length()>0)
                sb.append("&");

            sb.append(key + "=" + value);
            Log.d(TAG, sb.toString());
        }
        return sb.toString();
    }
}
