package com.roamtech.android.smsleo.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ScheduledmessagesListLazyAdapter extends RecyclerView.Adapter<ScheduledmessagesListLazyAdapter.ScheduledMessagesHolder> {
    public static final String TAG = "ScheduledmessagesListLazyAdapter";
    private Context ctx;
    private ArrayList<HashMap<String, String>> data;
    Fragment fragment;

    public ScheduledmessagesListLazyAdapter(Context ctx, ArrayList<HashMap<String, String>> d,Fragment fragment) {
        this.ctx = ctx;
        this.fragment = fragment;
        data=d;
    }

    public int getCount() {
        if(data == null){
            return 0;
        }else {
            return data.size();
        }
    }

    public Object getItem(int position) {
        return position;
    }

    @Override
    public ScheduledmessagesListLazyAdapter.ScheduledMessagesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_sent, parent, false);
        ScheduledMessagesHolder smh = new ScheduledMessagesHolder(v,ctx,fragment);
        return smh;
    }

    @Override
    public void onBindViewHolder(ScheduledmessagesListLazyAdapter.ScheduledMessagesHolder holder, int position) {
        holder.txtClientGroup.setText(data.get(position).get(BulkSMSConstant.TIME_SENT));
        holder.txtNumberSent.setText(data.get(position).get(BulkSMSConstant.APPROVED_BY));
        holder.txtMsg.setText(data.get(position).get(BulkSMSConstant.MESSAGE));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ScheduledMessagesHolder extends RecyclerView.ViewHolder{
        TextView txtClientGroup;
        TextView txtMsg;
        TextView txtNumberSent;
        Fragment fragment;
        Context ctx;

        public ScheduledMessagesHolder(View itemView,Context ctx,Fragment fragment) {
            super(itemView);
            this.ctx = ctx;
            this.fragment = fragment;

            txtClientGroup = (TextView) itemView.findViewById(R.id.client_group);
            txtMsg = (TextView) itemView.findViewById(R.id.msg);
            txtNumberSent = (TextView) itemView.findViewById(R.id.no_sent);
        }
    }
}