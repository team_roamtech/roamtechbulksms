package com.roamtech.android.smsleo.models;

/**
 * Created by dennis on 6/30/15.
 */
public class LoginItems {
    public boolean isUser;
    public String  clientID;

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public boolean isUser() {
        return isUser;
    }

    public void setUser(boolean isUser) {
        this.isUser = isUser;
    }
}
