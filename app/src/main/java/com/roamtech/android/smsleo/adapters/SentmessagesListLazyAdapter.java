package com.roamtech.android.smsleo.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SentmessagesListLazyAdapter extends RecyclerView.Adapter<SentmessagesListLazyAdapter.SentMessagesHolder> {
    public static final String TAG = "SentmessagesListLazyAdapter";
    private Context ctx;
    private ArrayList<HashMap<String, String>> data;
    Fragment fragment;
    
    public SentmessagesListLazyAdapter(Context ctx, ArrayList<HashMap<String, String>> d,Fragment fragment) {
        this.ctx = ctx;
        this.fragment = fragment;
        data=d;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public SentMessagesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_sent, parent, false);
        SentMessagesHolder smh = new SentMessagesHolder(v,ctx,fragment);
        return smh;
    }

    @Override
    public void onBindViewHolder(SentMessagesHolder holder, int position) {
        holder.txtClientGroup.setText(data.get(position).get(BulkSMSConstant.TIME_SENT));
        holder.txtNumberSent.setText(data.get(position).get(BulkSMSConstant.APPROVED_BY));
        holder.txtMsg.setText(data.get(position).get(BulkSMSConstant.MESSAGE));
    }

    public static class SentMessagesHolder extends RecyclerView.ViewHolder{
        TextView txtClientGroup;
        TextView txtMsg;
        TextView txtNumberSent;
        Fragment fragment;
        Context ctx;

        public SentMessagesHolder(View itemView,Context ctx,Fragment fragment) {
            super(itemView);
            this.ctx = ctx;
            this.fragment = fragment;

            txtClientGroup = (TextView) itemView.findViewById(R.id.client_group);
            txtMsg = (TextView) itemView.findViewById(R.id.msg);
            txtNumberSent = (TextView) itemView.findViewById(R.id.no_sent);
        }
    }
}