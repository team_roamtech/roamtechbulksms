package com.roamtech.android.smsleo;



import android.os.Bundle;
import android.preference.PreferenceManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;

import com.roamtech.android.smsleo.R;

public class SplashActivity extends AppCompatActivity {
	public static final String TAG = "SplashActivity";
	
	/**
	 * The thread to process splash screen events
	 */
	private Thread mSplashThread;
	SharedPreferences app_preference;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Splash screen view
		setContentView(R.layout.activity_splash);
		app_preference = PreferenceManager.getDefaultSharedPreferences(this);

		final SplashActivity sPlashScreen = this;

		// The thread to wait for splash screen events
		mSplashThread = new Thread() {
			@Override
			public void run() {
				try {
					synchronized (this) {
						// Wait given period of time or exit on touch
						wait(5000);
					}
				} catch (InterruptedException ex) {
				}

				finish();

				if (app_preference.contains(getString(R.string.bundle_username))
						&& app_preference.contains(getString(R.string.bundle_password))) {
					// Run next activity
					Log.d(TAG, "client id = "+app_preference.getString(getString(R.string.bundle_client_id), null));
					Intent intent = new Intent();
					intent.setClass(sPlashScreen, HomeActivity2.class);
					startActivity(intent);
					finish();
				}else{
					Intent intent = new Intent();
					intent.setClass(sPlashScreen, LoginActivity.class);
					startActivity(intent);
					finish();
				}
			}
		};

		mSplashThread.start();
	}

	/**
	 * Processes splash screen touch events
	 */
	@Override
	public boolean onTouchEvent(MotionEvent evt) {
		if (evt.getAction() == MotionEvent.ACTION_DOWN) {
			synchronized (mSplashThread) {
				mSplashThread.notifyAll();
			}
		}
		return true;
	}

}
