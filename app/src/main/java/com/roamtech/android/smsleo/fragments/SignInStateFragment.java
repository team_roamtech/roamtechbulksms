package com.roamtech.android.smsleo.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.adapters.SigninPagerAdapter;
import com.roamtech.android.smsleo.util.AsyncLoader;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignInStateFragment extends Fragment implements LoaderManager.LoaderCallbacks<int[]>{
    AppCompatActivity mActivity;
    ViewPager mPager;
    SigninPagerAdapter mAdapter;
    public static final int LOADER = 1005;
    int [] pagerItemsArray = {0,1,2};


    public SignInStateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sign_in_state, container, false);
        mPager = (ViewPager) rootView.findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(0);
        if(pagerItemsArray != null && pagerItemsArray.length > 1){
            mAdapter = new SigninPagerAdapter(mActivity,getChildFragmentManager());
            if(mAdapter != null) mPager.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            mPager.setCurrentItem(0, true);
        }else {
            getLoaderManager().restartLoader(LOADER, null, this);
        }

        return rootView;
    }

    public void addScrollToFragment(int position){
        mPager.setCurrentItem(position, true);
    }

    public static class GetFragments extends AsyncLoader<int[]> {
        int [] arrayItems;
        Context ctx;

        public GetFragments(Context context,int [] arrayItems) {
            super(context);
            // TODO Auto-generated constructor stub
            this.ctx = context;
            this.arrayItems = arrayItems;

        }

        @Override
        public int[] loadInBackground() {
            return arrayItems;
        }
        //Process the JSON result to Data we can use
    }


    @Override
    public Loader<int[]> onCreateLoader(int id, Bundle args) {
        return new GetFragments(getActivity(),pagerItemsArray);
    }

    @Override
    public void onLoadFinished(Loader<int[]> loader, int[] data) {
        mAdapter = new SigninPagerAdapter(mActivity,getChildFragmentManager());
        if(mAdapter != null) mPager.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        mPager.setCurrentItem(0, true);
    }

    @Override
    public void onLoaderReset(Loader<int[]> loader) {

    }


}
