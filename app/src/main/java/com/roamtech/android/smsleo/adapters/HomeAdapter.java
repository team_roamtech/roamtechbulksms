package com.roamtech.android.smsleo.adapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.roamtech.android.smsleo.R;

/**
 * Created by Karuri on 2/22/2015.
 */
public class HomeAdapter extends ArrayAdapter<String> {
    Context context;
    private LayoutInflater vi;
    HomeHolder holder;
    TypedArray typedArray;
    String [] homeItems;

    public HomeAdapter(Context context, String [] homeItems, TypedArray typedArray) {
        super(context, 0, homeItems);
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.homeItems = homeItems;
        this.typedArray = typedArray;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return homeItems.length;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        holder = new HomeHolder();
        View v = vi.inflate(R.layout.list_home_items, null);
            holder.txtTitle = (TextView) v.findViewById(R.id.txt_home_item);
            holder.txtTitle.setText(homeItems[position]);
            holder.imgResource = (ImageView) v.findViewById(R.id.img_home_item);
            holder.imgResource.setImageResource(typedArray.getResourceId(position, -1));
        return v;
    }

    static class HomeHolder {
        TextView txtTitle;
        ImageView imgResource;
    }
}