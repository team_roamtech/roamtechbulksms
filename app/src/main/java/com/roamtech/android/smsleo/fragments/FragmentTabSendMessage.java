package com.roamtech.android.smsleo.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.adapters.SpinnerGroupAdapter;
import com.roamtech.android.smsleo.async_tasks.SendCreditBalanceTask;
import com.roamtech.android.smsleo.async_tasks.SendScheduledMessagesTask;
import com.roamtech.android.smsleo.dialog_fragments.SetDateDialogFragment;
import com.roamtech.android.smsleo.http.NetworkConnectionStatus;
import com.roamtech.android.smsleo.http.RestClient2;
import com.roamtech.android.smsleo.interfaces.ReturnResult;
import com.roamtech.android.smsleo.util.AsyncLoader;
import com.roamtech.android.smsleo.util.BulkSMSConstant;
import com.roamtech.android.smsleo.util.CommonUtils;
import com.roamtech.android.smsleo.widgets.DateTimePicker;
import com.roamtech.android.smsleo.widgets.DateTimePicker.ICustomDateTimeListener;

public class FragmentTabSendMessage extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<HashMap<String, String>>>,SwipeRefreshLayout.OnRefreshListener,
        ReturnResult{
    public static final String TAG = "FragmentTabSendMessage";
    Spinner spinnergroups;
	EditText txtDateDue, txtSendmessage;
    public ArrayList<HashMap<String, String>> contacts_list;
	private DateTimePicker dateTimePicker;
	Button btnsend, btnclear;
	Date d;
	Boolean boolCheckCredit = false;
	SharedPreferences app_preference;
	SharedPreferences.Editor editor;
    AppCompatActivity mActivity;
	int count_groups;
    CommonUtils commonUtils;
    SpinnerGroupAdapter spinnerGroupAdapter;
    public static final int LOADER = 1000;
    ProgressDialog progressDialog;
    String strMessage,strClientID,strServiceID,strServiceName,strSubCount,strSendTime;
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SetDateDialogFragment newDialogFragment;
    String setDate;
    HashMap<String, String> valuePairs = new HashMap<String, String>();
    Boolean isVisible;
    public static String messageResult;
    double balance,subCount;
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BulkSMSConstant.signedOut(mActivity);
        app_preference = PreferenceManager.getDefaultSharedPreferences(mActivity);
        editor = app_preference.edit();
        commonUtils = new CommonUtils();
        contacts_list = new ArrayList<HashMap<String, String>>();
        newDialogFragment = new SetDateDialogFragment();
        strClientID = app_preference.getString(mActivity.getResources().getString(R.string.bundle_customer_account), null);
    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_sendmessages, container, false);
		BulkSMSConstant.signedOut(getActivity());
		spinnergroups = (Spinner) view.findViewById(R.id.spinnergroups);
		txtSendmessage = (EditText) view.findViewById(R.id.txtSendmessage);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_container);
        mSwipeRefreshLayout.setColorSchemeColors(R.color.blue_dark_theme,R.color.blueback,R.color.mycolor);
        mSwipeRefreshLayout.setOnRefreshListener(this);

		//roamtechgroups();
		spinnergroups.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnergroups.setSelection(position);
                strServiceID = contacts_list.get(position).get(BulkSMSConstant.SERVICE_ID);
                strServiceName = contacts_list.get(position).get(BulkSMSConstant.SERVICE_NAME);
                strSubCount = contacts_list.get(position).get(BulkSMSConstant.SUBCOUNT);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }

        });
        if(contacts_list != null) {
            spinnerGroupAdapter = new SpinnerGroupAdapter(mActivity, 0, contacts_list);
            spinnergroups.setAdapter(spinnerGroupAdapter);
        }

		dateTimePicker = new DateTimePicker(mActivity, new ICustomDateTimeListener() {

			@Override
			public void onSet(Calendar calendarSelected, Date dateSelected, int year, String monthFullName, String monthShortName, int monthNumber, int date,
					String weekDayFullName, String weekDayShortName, int hour24, int hour12, int min, int sec, String AM_PM) {
				txtDateDue.setText("" + dateSelected);
				d = dateSelected;
			}

			@Override
			public void onCancel() {
			}
		});
		dateTimePicker.set24HourFormat(false);

		txtDateDue = (EditText) view.findViewById(R.id.txtsendtime);
		txtDateDue.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
                newDialogFragment.setTargetFragment(FragmentTabSendMessage.this, BulkSMSConstant.DIALOG_FRAGMENT_SET_DATE);
                newDialogFragment.show(commonUtils.addDialogFragment(mActivity.getSupportFragmentManager(), "dialog"), "dialog");
			}
		});

		btnsend = (Button) view.findViewById(R.id.btnsend);
		btnsend.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
                if (NetworkConnectionStatus.isOnline(mActivity)) {
                    if ((setDate != null) & txtSendmessage.getText() != null) {
                        progressDialog = new ProgressDialog(mActivity);
                        progressDialog.setMessage(mActivity.getResources().getString(R.string.dialog_sending));
                        progressDialog.show();
                        new SendCreditBalanceTask(mActivity, FragmentTabSendMessage.this, BulkSMSConstant.FLAG_CREDIT_BALANCE).execute(prepareValuePairs2());
                    } else {
                        Toast.makeText(getActivity(), mActivity.getResources().getString(R.string.toast_check_time_message), Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_connection),Toast.LENGTH_LONG).show();
                }
            }
		});

		btnclear = (Button) view.findViewById(R.id.btnclearsend);
		btnclear.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				txtSendmessage.setText("");
				txtDateDue.setText("");
			}
		});
		return view;
	}

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible == true) {
            if(app_preference.contains(BulkSMSConstant.GROUP_LIST)){
                String groupsList = app_preference.getString(BulkSMSConstant.GROUP_LIST,null);
                if(!groupsList.equals(null)){
                    contacts_list = parseJson(groupsList);
                }
            }

            if(NetworkConnectionStatus.isOnline(mActivity)) {
                mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this).forceLoad();
            }else{
                Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_connection),Toast.LENGTH_LONG).show();
            }
        }
    }

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

	}

    @Override
    public void onRefresh() {
        if(NetworkConnectionStatus.isOnline(mActivity)) {
            mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
        }else{
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_connection),Toast.LENGTH_LONG).show();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BulkSMSConstant.DIALOG_FRAGMENT_SET_DATE) {
            if (resultCode == Activity.RESULT_OK) {
                if(data.getExtras() != null) {
                    setDate = data.getStringExtra(mActivity.getResources().getString(R.string.bundle_set_date));
                    Log.d(TAG,setDate);
                    txtDateDue.setText(setDate);
                }
            }
        }
    }

    @Override
    public void onStartTask() {

    }

    @Override
    public void onReturnResult(Object result) {
        String strResult = (String) result;
        processCredits(strResult);
    }

    public void processCredits(String result){
//        Log.d(TAG,result);
        JSONObject jsonMessage = null;
        if(result != null) {
            jsonMessage = BulkSMSConstant.parseJson(result);
            if(jsonMessage != null){
                processJSONResult(jsonMessage);
            }else{
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_error_retrieving), Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_error_retrieving), Toast.LENGTH_LONG).show();
        }
        progressDialog.dismiss();
    }

    public void processJSONResult(JSONObject response){
        JSONObject jsonResult = null;
        try {
            if (response.has(BulkSMSConstant.CREDIT) & !response.isNull(BulkSMSConstant.CREDIT)) {
                jsonResult = response.getJSONObject(BulkSMSConstant.CREDIT);
                if(jsonResult.has(BulkSMSConstant.BALANCE) & !jsonResult.isNull(BulkSMSConstant.BALANCE)) {
                    balance = jsonResult.getDouble(BulkSMSConstant.BALANCE);
                    boolCheckCredit = checkCredit(balance);
                    if(boolCheckCredit){
                        if (NetworkConnectionStatus.isOnline(mActivity)) {
                            new SendScheduledMessagesTask(mActivity, FragmentTabSendMessage.this, BulkSMSConstant.FLAG_SEND_MESSAGE).execute(prepareValuePairs());
                        }else{
                            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_connection),Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_insufficient_credit),Toast.LENGTH_LONG).show();
                    }
                }
                }else if(response.has(BulkSMSConstant.ALERTS) & !response.isNull(BulkSMSConstant.ALERTS)) {
                jsonResult = response.getJSONObject(BulkSMSConstant.ALERTS);
                if(jsonResult.has(BulkSMSConstant.STATUS) & !jsonResult.isNull(BulkSMSConstant.STATUS)) {
                    messageResult = jsonResult.getString(BulkSMSConstant.STATUS);
                    txtDateDue.setText("");
                    txtSendmessage.setText("");
                    Toast.makeText(mActivity, messageResult, Toast.LENGTH_LONG).show();
                }
                }else if(response.has(BulkSMSConstant.MESSAGE) & !response.isNull(BulkSMSConstant.MESSAGE)) {
                messageResult =  response.getString(BulkSMSConstant.MESSAGE);
                Toast.makeText(mActivity,messageResult,Toast.LENGTH_LONG).show();


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean checkCredit(double balance){
        boolean boolCheckCredit;
        subCount = Double.parseDouble(strSubCount);
        if(balance >= subCount){
            boolCheckCredit = true;
        }else{
            boolCheckCredit = false;
        }
        return boolCheckCredit;
    }


    public static class GetGroups extends AsyncLoader<ArrayList<HashMap<String, String>>> {

        Context ctx;
        public ArrayList<HashMap<String, String>> groups_list = new ArrayList<HashMap<String, String>>();
        HashMap<String,String> hashMap = new HashMap<String,String>();
        String app_preference;
        SharedPreferences prefs;

        public GetGroups(Context context, String app_preference,SharedPreferences prefs) {
            super(context);
            // TODO Auto-generated constructor stub
            this.ctx = context;
            this.app_preference = app_preference;
            this.prefs = prefs;
        }

        @Override
        public ArrayList<HashMap<String, String>> loadInBackground() {
            Log.d(TAG, "  loader launched ");
            String response = null;
            JSONObject jsonMessage = null;
            try {
                hashMap.put(BulkSMSConstant.REQUEST, BulkSMSConstant.GROUP);
                hashMap.put(BulkSMSConstant.CLIENT_ID, app_preference);
                Log.d(TAG, app_preference);
                response = RestClient2.makeRestRequest(RestClient2.GET, BulkSMSConstant.SITE_URL2, hashMap);
                 Log.d(TAG, "------- " +response);
                if(response != null) {
                    jsonMessage = BulkSMSConstant.parseJson(response);
                    if(jsonMessage != null){
                        groups_list = parseJson(jsonMessage.toString());
                        if(groups_list != null){
                            if(prefs.contains(BulkSMSConstant.GROUP_LIST)){
                                prefs.edit().remove(BulkSMSConstant.GROUP_LIST).commit();
                            }
                            prefs.edit().putString(BulkSMSConstant.GROUP_LIST, jsonMessage.toString()).commit();
                        }
                    }else{
                        groups_list = null;
                    }
                }else{
                    groups_list = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            return groups_list;
        }
    }

    public static ArrayList<HashMap<String, String>> parseJson(String strResponse) {
        JSONObject response =  null;
        String message = null;
        ArrayList<HashMap<String, String>> groups_list = new ArrayList<HashMap<String, String>>();
        try {
            response = new JSONObject(strResponse);
            if (response.has(BulkSMSConstant.GROUPS) & !response.isNull(BulkSMSConstant.GROUPS)) {
                JSONArray jsonGroups = new JSONArray(response.getString(BulkSMSConstant.GROUPS));
                JSONObject c;
                for (int i = 0; i < jsonGroups.length(); i++) {
                    c = jsonGroups.getJSONObject(i);
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(BulkSMSConstant.SUBCOUNT, c.getString(BulkSMSConstant.SUBCOUNT));
                    map.put(BulkSMSConstant.SERVICE_ID, c.getString(BulkSMSConstant.SERVICE_ID));
                    map.put(BulkSMSConstant.SERVICE_NAME, c.getString(BulkSMSConstant.SERVICE_NAME));
                    groups_list.add(map);
                }
            }else if(response.has(BulkSMSConstant.MESSAGE) & !response.isNull(BulkSMSConstant.MESSAGE)){
                message =  response.getString(BulkSMSConstant.MESSAGE);
                messageResult = message;
                groups_list = null;
            }else {
                groups_list = null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            groups_list = null;
        }
        return groups_list;
    }

    @Override
    public Loader<ArrayList<HashMap<String, String>>> onCreateLoader(int i, Bundle bundle) {
        if(i == LOADER) {
            if(!app_preference.contains(BulkSMSConstant.GROUP_LIST)) {
                progressDialog = new ProgressDialog(mActivity);
                progressDialog.setMessage("Loading ...");
                progressDialog.show();
            }
            return new GetGroups(mActivity, app_preference.getString(mActivity.getResources().getString(R.string.bundle_customer_account), ""),app_preference);
        }
        return  null;
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<HashMap<String, String>>> arrayListLoader, ArrayList<HashMap<String, String>> hashMaps) {
        if(arrayListLoader.getId() == LOADER) {
            contacts_list = hashMaps;
            if (contacts_list != null) {
                spinnerGroupAdapter = new SpinnerGroupAdapter(mActivity, 0, contacts_list);
                spinnergroups.setAdapter(spinnerGroupAdapter);
            } else if (messageResult != null && contacts_list == null) {
                Toast.makeText(mActivity, messageResult, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_error_retrieving), Toast.LENGTH_LONG).show();
            }
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<HashMap<String, String>>> arrayListLoader) {

    }

    public HashMap<String, String>  prepareValuePairs(){
        strMessage = txtSendmessage.getText().toString();
        //strClientID = "CC40";
        valuePairs.put(BulkSMSConstant.MESSAGE,strMessage);
        valuePairs.put(BulkSMSConstant.CLIENT_ID,strClientID);
        valuePairs.put(BulkSMSConstant.SEND_TIME,setDate);
        valuePairs.put(BulkSMSConstant.SERVICE_ID,strServiceID);
        valuePairs.put(BulkSMSConstant.SUBCOUNT, strSubCount);
        valuePairs.put(BulkSMSConstant.REQUEST, BulkSMSConstant.ALERT);
        return valuePairs;
    }

    public HashMap<String, String>  prepareValuePairs2(){
        valuePairs.put(BulkSMSConstant.CLIENT_ID,strClientID);
        valuePairs.put(BulkSMSConstant.REQUEST,BulkSMSConstant.CREDIT);
        return valuePairs;
    }


    public String getTime(){
        Calendar c = Calendar.getInstance();
        strSendTime  = df.format(c.getTime());
        return strSendTime;
    }
}
