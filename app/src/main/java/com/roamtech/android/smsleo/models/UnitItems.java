package com.roamtech.android.smsleo.models;

/**
 * Created by dennis on 8/20/15.
 */
public class UnitItems {
    int firstItem;
    int secondItem;

    public UnitItems(){

    }

    public int getFirstItem() {
        return firstItem;
    }

    public void setFirstItem(int firstItem) {
        this.firstItem = firstItem;
    }

    public int getSecondItem() {
        return secondItem;
    }

    public void setSecondItem(int secondItem) {
        this.secondItem = secondItem;
    }
}