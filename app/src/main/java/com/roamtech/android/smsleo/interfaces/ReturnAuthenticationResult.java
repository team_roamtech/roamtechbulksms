package com.roamtech.android.smsleo.interfaces;

import com.roamtech.android.smsleo.models.LoginItems;

/**
 * Created by dennis on 6/30/15.
 */
public interface ReturnAuthenticationResult {
    void onStartTask();
    void onReturnResult(String result,LoginItems loginItems);
}
