package com.roamtech.android.smsleo.dialog_fragments;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.roamtech.android.smsleo.HomeActivity2;
import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.interfaces.CheckConfirm;
import com.roamtech.android.smsleo.util.CommonUtils;

/**
 * Created by dennis on 5/25/15.
 */
public class ConfirmDialog extends DialogFragment implements View.OnClickListener {
    public static final String TAG = "SignOutDialog";
    AppCompatActivity mActivity;
    Button btnYes,btnNo;
    TextView txtWarning;
    CheckConfirm mCallBack;
    String msgText,returnTag;

    public ConfirmDialog() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            mCallBack = (CheckConfirm) activity;
        }catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement OnHeadlineSelectedListener");
            }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if(args != null){
            msgText = args.getString(CommonUtils.SIGNOUT);
            returnTag = args.getString(CommonUtils.RETURN_TAG);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.dialog_confirm);
        dialog.show();
        dialog.setTitle(mActivity.getResources().getString(R.string.action_sign_out));
        dialog.setCanceledOnTouchOutside(false);
        txtWarning = (TextView) dialog.findViewById(R.id.txt_warning);
        txtWarning.setText(msgText);
        btnYes = (Button) dialog.findViewById(R.id.btn_yes);
        btnYes.setOnClickListener(this);
        btnNo = (Button) dialog.findViewById(R.id.btn_no);
        btnNo.setOnClickListener(this);
        return dialog;
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_yes:
                mCallBack.confirmYes(returnTag);
                dismiss();
                break;
            case R.id.btn_no:
                mCallBack.confirmNo(returnTag);
                dismiss();
                break;
        }

    }
}
