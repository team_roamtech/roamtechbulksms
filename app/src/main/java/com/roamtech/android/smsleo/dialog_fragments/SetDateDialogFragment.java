package com.roamtech.android.smsleo.dialog_fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.ViewSwitcher;

import com.roamtech.android.smsleo.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by dennis on 6/22/15.
 */
public class SetDateDialogFragment extends DialogFragment implements View.OnClickListener {
    public static final String TAG = "SetDateDialogFragment";
    AppCompatActivity mActivity;
    SimpleDateFormat dateFormat;
    DatePicker dtpDate;
    TimePicker tpTime;
    Button btnSet,btnCancel,btnSetDate,btnSetTime;
    ViewSwitcher viewSwitcher;
    Animation slide_in_left, slide_out_right;
    String setDate;

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_set:
                setDate = dateFormat.format(getDateFromDatePicker(dtpDate,tpTime));
                Intent intent = new Intent();
                intent.putExtra(mActivity.getResources().getString(R.string.bundle_set_date), setDate);
                mActivity.setResult(Activity.RESULT_OK, intent);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                dismiss();
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
            case R.id.btn_set_date:
                viewSwitcher.showPrevious();
                break;
            case R.id.btn_set_time:
                viewSwitcher.showNext();
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.dialog_set_date);
        dialog.show();
       // dialog.setTitle(mActivity.getResources().getString(R.string.dialog_title_date));
        dialog.setCanceledOnTouchOutside(false);
        dtpDate = (DatePicker) dialog.findViewById(R.id.dtp_date);
        tpTime = (TimePicker) dialog.findViewById(R.id.ttp_time);
        btnSet = (Button) dialog.findViewById(R.id.btn_set);
        btnSet.setOnClickListener(this);
        btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
        btnSetDate = (Button) dialog.findViewById(R.id.btn_set_date);
        btnSetDate.setOnClickListener(this);
        btnSetTime = (Button) dialog.findViewById(R.id.btn_set_time);
        btnSetTime.setOnClickListener(this);
        viewSwitcher = (ViewSwitcher) dialog.findViewById(R.id.viewswitcher);

        slide_in_left = AnimationUtils.loadAnimation(mActivity,
                android.R.anim.slide_in_left);
        slide_out_right = AnimationUtils.loadAnimation(mActivity,
                android.R.anim.slide_out_right);

        viewSwitcher.setInAnimation(slide_in_left);
        viewSwitcher.setOutAnimation(slide_out_right);
        return dialog;
    }

    public Date getDateFromDatePicker(DatePicker datePicker,TimePicker timePicker){
        int year =  datePicker.getYear();
        int month = datePicker.getMonth();
        int day = datePicker.getDayOfMonth();
        int hours = timePicker.getCurrentHour();
        int minutes = timePicker.getCurrentMinute();
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day,hours,minutes);
        return calendar.getTime();
    }

}
