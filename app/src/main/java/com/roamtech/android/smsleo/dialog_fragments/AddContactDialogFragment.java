package com.roamtech.android.smsleo.dialog_fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.async_tasks.SendScheduledMessagesTask;
import com.roamtech.android.smsleo.http.NetworkConnectionStatus;
import com.roamtech.android.smsleo.interfaces.ReturnResult;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by dennis on 6/23/15.
 */
public class AddContactDialogFragment extends DialogFragment implements View.OnClickListener,ReturnResult {
    public static final String TAG = "AddContactDialogFragment";
    AppCompatActivity mActivity;
    Button btnAdd, btnDone;
    EditText editNum;
    String strServiceID, strSubCount, strServiceName, strPhones, strPhones2, strClientID;
    ProgressDialog progressDialog;
    HashMap<String, String> valuePairs = new HashMap<String, String>();
    SharedPreferences app_preference;
    SharedPreferences.Editor editor;
    String messageResult,info;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            strServiceID = args.getString(BulkSMSConstant.SERVICE_ID);
            strSubCount = args.getString(BulkSMSConstant.SUBCOUNT);
            strServiceName = args.getString(BulkSMSConstant.SERVICE_NAME);
        }
        app_preference = PreferenceManager.getDefaultSharedPreferences(mActivity);
        editor = app_preference.edit();
        strPhones = "";
        strClientID = app_preference.getString(mActivity.getResources().getString(R.string.bundle_customer_account), null);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.dialog_add_contact);
        dialog.setTitle(mActivity.getResources().getString(R.string.add_new_contacts));
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
        editNum = (EditText) dialog.findViewById(R.id.edit_num);
        btnAdd = (Button) dialog.findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);
        btnDone = (Button) dialog.findViewById(R.id.btn_done);
        btnDone.setOnClickListener(this);
        return dialog;
    }

    public String createJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(BulkSMSConstant.PHONE, strPhones);
            jsonObject.put(BulkSMSConstant.SERVICE_ID, strServiceID);
            jsonObject.put(BulkSMSConstant.CLIENT_ID, "CC40");
            jsonObject.put(BulkSMSConstant.SUBCOUNT, strSubCount);
            jsonObject.put(BulkSMSConstant.SERVICE_NAME, strServiceName);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add:
                if(editNum.getText().length() > 1) {
                    if (editNum.getText().length() == 12 & editNum.getText().toString().startsWith("254")) {
                        strPhones2 = editNum.getText().toString();
                        if(NetworkConnectionStatus.isOnline(mActivity)) {
                            new SendScheduledMessagesTask(mActivity, AddContactDialogFragment.this, BulkSMSConstant.FLAG_ADD_CONTACTS).execute(prepareValuePairs());
                        }else{
                            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_connection),Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_enter_valid_number), Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_enter_details), Toast.LENGTH_LONG).show();
                    }
                break;
            case R.id.btn_done:
                if(editNum.getText().length() > 1) {
                    if (editNum.getText().length() == 12 & editNum.getText().toString().startsWith("254")) {
                        strPhones2 = editNum.getText().toString();
                        if(NetworkConnectionStatus.isOnline(mActivity)) {
                            new SendScheduledMessagesTask(mActivity, AddContactDialogFragment.this, BulkSMSConstant.FLAG_ADD_CONTACTS).execute(prepareValuePairs());
                        }else{
                            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_connection),Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_enter_valid_number), Toast.LENGTH_LONG).show();
                    }
                }else{
                    Intent intent = new Intent();
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    Log.d(TAG,String.valueOf(getTargetRequestCode()));
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                    dismiss();
                }
                Intent intent = new Intent();
                mActivity.setResult(Activity.RESULT_OK, intent);
                Log.d(TAG,String.valueOf(getTargetRequestCode()));
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                dismiss();
                break;
        }
    }

    @Override
    public void onStartTask() {
        progressDialog = new ProgressDialog(mActivity);
        progressDialog.setMessage("Sending ...");
        progressDialog.show();
    }

    @Override
    public void onReturnResult(Object result) {
        String strResult = (String) result;
        processResponse(strResult);
    }

    public void processResponse(String result){
        JSONObject jsonMessage = null;
        if(result != null) {
            jsonMessage = BulkSMSConstant.parseJson(result);
            if(jsonMessage != null){
                processJSONResult(jsonMessage);
            }else{
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_error_retrieving), Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_error_retrieving), Toast.LENGTH_LONG).show();
        }
        progressDialog.dismiss();
    }

    public void processJSONResult(JSONObject response){
        JSONObject jsonResult = null;
        try {
            if (response.has(BulkSMSConstant.CONTACT) & !response.isNull(BulkSMSConstant.CONTACT)) {
                jsonResult = response.getJSONObject(BulkSMSConstant.CONTACT);
                if(jsonResult.has(BulkSMSConstant.INFO) & !jsonResult.isNull(BulkSMSConstant.INFO)) {
                    info = jsonResult.getString(BulkSMSConstant.INFO);
                    Toast.makeText(mActivity, info, Toast.LENGTH_LONG).show();
                    editNum.setText("");
                    }else{
                        Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_error_retrieving),Toast.LENGTH_LONG).show();
                }
            }else if(response.has(BulkSMSConstant.MESSAGE) & !response.isNull(BulkSMSConstant.MESSAGE)) {
                messageResult =  response.getString(BulkSMSConstant.MESSAGE);
                Toast.makeText(mActivity,messageResult,Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_error_retrieving),Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public HashMap<String, String> prepareValuePairs(){
        valuePairs.put(BulkSMSConstant.CLIENT_ID,strClientID);
        valuePairs.put(BulkSMSConstant.GROUPID, strServiceID);
        valuePairs.put(BulkSMSConstant.NUMBER, strPhones2);
        valuePairs.put(BulkSMSConstant.REQUEST, BulkSMSConstant.CON2GROUP);
        return valuePairs;
    }

    public String parseJson(String json){
        String code = null;
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject json2 = jsonObject.getJSONObject(BulkSMSConstant.RESPONSE);
            JSONObject json3 = json2.getJSONObject(BulkSMSConstant.RESULT);
            code = json3.getString(BulkSMSConstant.CODE);
            Log.d(TAG,"code is "+code);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return code;

    }
}
