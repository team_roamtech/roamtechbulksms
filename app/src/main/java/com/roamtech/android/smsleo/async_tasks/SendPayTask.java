package com.roamtech.android.smsleo.async_tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.roamtech.android.smsleo.http.RestClient;
import com.roamtech.android.smsleo.http.RestClient2;
import com.roamtech.android.smsleo.interfaces.ReturnResult;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dennis on 8/13/15.
 */
public class SendPayTask extends AsyncTask<Object, Void, Object> {
    public static final String TAG = "SendPayTask";
    Context context;
    Fragment fragment;
    HashMap<String, String> valuePairs = new HashMap<String, String>();
    String json;
    int flag;

    public SendPayTask(Context context, Fragment fragment,int flag){
        this.context = context;
        this.fragment = fragment;
        this.flag = flag;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        ReturnResult ac = (ReturnResult) fragment;
        ac.onStartTask();
    }

    @Override
    protected Object doInBackground(Object... params) {
        String response = null;
        if(flag == BulkSMSConstant.FLAG_MPESA){
            valuePairs = (HashMap<String, String>) params[0];
        }
        try {
            response = RestClient2.makeRestRequest(RestClient2.POST, BulkSMSConstant.LIPANAMPESAURL, valuePairs);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(Object result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        ReturnResult ac = (ReturnResult) fragment;
        ac.onReturnResult(result);
    }
}
