package com.roamtech.android.smsleo.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.dialog_fragments.AddContactDialogFragment;
import com.roamtech.android.smsleo.http.RestClient;
import com.roamtech.android.smsleo.http.RestClient2;
import com.roamtech.android.smsleo.interfaces.LaunchResults;
import com.roamtech.android.smsleo.util.AsyncLoader;
import com.roamtech.android.smsleo.util.BulkSMSConstant;
import com.roamtech.android.smsleo.adapters.ManagecontactitemsListLazyAdapter;
import com.roamtech.android.smsleo.util.CommonUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManageContactsItemsListFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<HashMap<String, String>>>,SwipeRefreshLayout.OnRefreshListener,
        LaunchResults,PopupMenu.OnMenuItemClickListener{
    public static final String TAG = "ManageContactsItemsListFragment";
    AppCompatActivity mActivity;
    public static final String KEY_NO = "msisdn";
    public ArrayList<HashMap<String, String>> contacts_list;
    SharedPreferences app_preference;
    SharedPreferences.Editor editor;
    RecyclerView contactsRecycler;
    ManagecontactitemsListLazyAdapter adapter;
    String strServiceID, strSubCount, strServiceName;
    public static final int LOADER = 1000;
    SwipeRefreshLayout mSwipeRefreshLayout;
    public static String messageResult;
    ProgressBar mProgresBar;
    AddContactDialogFragment newDialogFragment1;
    CommonUtils commonUtils;
    int adapterPosition;

    public ManageContactsItemsListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        BulkSMSConstant.signedOut(mActivity);
        app_preference = PreferenceManager.getDefaultSharedPreferences(mActivity);
        editor = app_preference.edit();
        commonUtils = new CommonUtils();
        Bundle args = getArguments();
        if (args != null) {
            strServiceID = args.getString(BulkSMSConstant.SERVICE_ID);
            strSubCount = args.getString(BulkSMSConstant.SUBCOUNT);
            strServiceName = args.getString(BulkSMSConstant.SERVICE_NAME);
        }
        newDialogFragment1 = new AddContactDialogFragment();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_sentmessages, container, false);
        contactsRecycler = (RecyclerView) rootView.findViewById(R.id.lst_sent);
        LinearLayoutManager llm = new LinearLayoutManager(mActivity);
        contactsRecycler.setLayoutManager(llm);
        mProgresBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        contacts_list = new ArrayList<HashMap<String, String>>();
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh_container);
        mSwipeRefreshLayout.setColorSchemeColors(R.color.blue_dark_theme,R.color.blueback,R.color.mycolor);
        mSwipeRefreshLayout.setOnRefreshListener(this);

      //mActivity.getSupportActionBar().setTitle(groupName + " - Contacts");
       // mActivity.getSupportLoaderManager().initLoader(LOADER, null, this);

        //loopAsyncTask();

        //registerForContextMenu(list);
        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.listmanagecontactitems) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            menu.setHeaderTitle(contacts_list.get(info.position).get(KEY_NO));
            String[] menuItems = getResources().getStringArray(R.array.contactgroupitemmenu);
            for (int i = 0; i < menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(android.view.MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        // String[] menuItems =
        // getResources().getStringArray(R.array.contactgroupitemmenu);
        // String menuItemName = menuItems[menuItemIndex];
        // String listItemName = contacts_list.get(info.position).get(KEY_ID);

        if (menuItemIndex == 0) {

        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_add_contact:
                Bundle args = new Bundle();
                args.putString(BulkSMSConstant.SERVICE_ID, strServiceID);
                args.putString(BulkSMSConstant.SERVICE_NAME, strServiceName);
                args.putString(BulkSMSConstant.SUBCOUNT, strSubCount);
                newDialogFragment1.setArguments(args);
                newDialogFragment1.setTargetFragment(this, BulkSMSConstant.DIALOG_FRAGMENT_ADD_CONTACTS);
                newDialogFragment1.show(commonUtils.addDialogFragment(mActivity.getSupportFragmentManager(), "dialog"), "dialog");
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == BulkSMSConstant.DIALOG_FRAGMENT_ADD_CONTACTS ) {
            if (resultCode == Activity.RESULT_OK) {
                mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
            } else {

            }
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_add_contact).setVisible(true);
    }

    @Override
    public void onRefresh() {
        mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
    }

    @Override
    public void launchResults(int position, Bundle bundle, int view, View itemView) {
        adapterPosition = position;
        if(bundle != null){
            PopupMenu popupMenu = new PopupMenu(mActivity, itemView);
            popupMenu.setOnMenuItemClickListener(this);
            popupMenu.inflate(R.menu.pop_up_numbers_menu);
            popupMenu.show();
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Bundle args = new Bundle();
        switch (item.getItemId()) {
            case R.id.item_view_contact:

                return true;
            default:

                return false;

        }
    }

    public static class GetContacts extends AsyncLoader<ArrayList<HashMap<String, String>>> {
        Context ctx;
        public ArrayList<HashMap<String, String>> contacts_list = new ArrayList<HashMap<String, String>>();
        String app_preference;
        HashMap<String,String> hashMap = new HashMap<String,String>();
        String serviceID;

        public GetContacts(Context context,String app_preference,String serviceID) {
            super(context);
            // TODO Auto-generated constructor stub
            this.ctx = context;
            this.app_preference = app_preference;
            this.serviceID = serviceID;
        }

        @Override
        public ArrayList<HashMap<String, String>> loadInBackground() {
            String response = null;
            JSONObject jsonMessage = null;
            try {
                hashMap.put(BulkSMSConstant.CLIENT_ID, app_preference);
                hashMap.put(BulkSMSConstant.REQUEST, BulkSMSConstant.CON);
                hashMap.put(BulkSMSConstant.SERVICE_ID, serviceID);

                response = RestClient2.makeRestRequest(RestClient2.GET, BulkSMSConstant.SITE_URL2, hashMap);
                Log.d(TAG, "----------- "+response);
                if(response != null) {
                    jsonMessage = BulkSMSConstant.parseJson(response);
                    if(jsonMessage != null){
                        contacts_list = parseJson(jsonMessage);
                    }else{
                        contacts_list = null;
                    }
                }else{
                    contacts_list = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            return contacts_list;
        }

        public ArrayList<HashMap<String, String>> parseJson(JSONObject response){
            String message = null;
            try {
                if (response.has(BulkSMSConstant.CONTACTS) & !response.isNull(BulkSMSConstant.CONTACTS)) {
                    JSONArray jsonResult = new JSONArray(response.getString(BulkSMSConstant.CONTACTS));
                    JSONObject c;
                    for (int i = 0; i < jsonResult.length(); i++) {
                            c = jsonResult.getJSONObject(i);
                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put(BulkSMSConstant.SERVICE_ID, c.getString(BulkSMSConstant.SERVICE_ID));
                            map.put(BulkSMSConstant.MSISDN, c.getString(BulkSMSConstant.MSISDN));
                            map.put(BulkSMSConstant.SUBSCRIPTION_DATE, c.getString(BulkSMSConstant.SUBSCRIPTION_DATE));
                            map.put(BulkSMSConstant.CLIENT_ID, c.getString(BulkSMSConstant.CLIENT_ID));
                            contacts_list.add(map);
                        }
                    } else if (response.has(BulkSMSConstant.MESSAGE) & !response.isNull(BulkSMSConstant.MESSAGE)) {
                        message = response.getString(BulkSMSConstant.MESSAGE);
                        messageResult = message;
                        contacts_list = null;
                }else{
                    contacts_list = null;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return contacts_list;
        }

    }

    @Override
    public Loader<ArrayList<HashMap<String, String>>> onCreateLoader(int i, Bundle bundle) {
        mProgresBar.setVisibility(View.VISIBLE);
        return new GetContacts(mActivity,app_preference.getString(mActivity.getResources().getString(R.string.bundle_customer_account), ""),strServiceID);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<HashMap<String, String>>> arrayListLoader, ArrayList<HashMap<String, String>> hashMaps) {
        contacts_list = hashMaps;
        if(contacts_list != null) {
            adapter =  new ManagecontactitemsListLazyAdapter(mActivity,
                    contacts_list,this);
            contactsRecycler.setAdapter(adapter);
        }else if(contacts_list == null & messageResult != null){
            Toast.makeText(mActivity, messageResult, Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(mActivity,messageResult,Toast.LENGTH_LONG).show();
        }
        mSwipeRefreshLayout.setRefreshing(false);
        mProgresBar.setVisibility(View.GONE);

    }

    @Override
    public void onLoaderReset(Loader<ArrayList<HashMap<String, String>>> arrayListLoader) {

    }
}
