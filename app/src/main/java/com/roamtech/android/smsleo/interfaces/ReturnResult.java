package com.roamtech.android.smsleo.interfaces;

/**
 * Created by dennis on 6/22/15.
 */
public interface ReturnResult {
    void onStartTask();
    void onReturnResult(Object result);
}
