package com.roamtech.android.smsleo.adapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.interfaces.LaunchResults;

/**
 * Created by dennis on 2/17/15.
 */
public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.DrawerHolder> {
    public static final String TAG = "DrawerAdapter";
    String [] drawerItems;
    private LayoutInflater vi;
    TypedArray typedArray;
    Context context;

    public DrawerAdapter(Context context, String[] drawerItems, TypedArray typedArray) {
        this.context = context;
        this.drawerItems = drawerItems;
        this.typedArray = typedArray;
    }

    @Override
    public DrawerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_drawer_items, parent, false);
        DrawerHolder dmh = new DrawerHolder(v,context);
        return dmh;

    }

    @Override
    public void onBindViewHolder(DrawerHolder holder, int position) {
        holder.imgMenuItem.setImageResource(typedArray.getResourceId(position,-1));
        Log.d(TAG, drawerItems[position]);
        holder.txtMenuItem.setText(drawerItems[position]);
    }

    @Override
    public int getItemCount() {
        return drawerItems.length;
    }

    public static class DrawerHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView imgMenuItem;
        public TextView txtMenuItem;
        public LinearLayout lytDrawer;
        Context ctx;
        LaunchResults mCallBack;

        public DrawerHolder(View itemView,Context ctx){
            super(itemView);
            this.ctx = ctx;
            try {
                mCallBack = (LaunchResults) ctx;
            } catch (ClassCastException e) {
                throw new ClassCastException(ctx.toString()
                        + " must implement OnHeadlineSelectedListener");
            }
            txtMenuItem = (TextView) itemView.findViewById(R.id.txt_menu_item);
            imgMenuItem = (ImageView) itemView.findViewById(R.id.img_menu_item);
            lytDrawer = (LinearLayout) itemView.findViewById(R.id.lyt_drawer_items);
            lytDrawer.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.lyt_drawer_items:
                    mCallBack.launchResults(getAdapterPosition(),null,R.id.lyt_drawer_items,v);
                    break;
                default:
                    break;
            }
        }
    }

}
