package com.roamtech.android.smsleo.async_tasks;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;


import com.roamtech.android.smsleo.client.LDAPServerInstance;
import com.roamtech.android.smsleo.http.RestClient;
import com.roamtech.android.smsleo.interfaces.ReturnAuthenticationResult;
import com.roamtech.android.smsleo.models.LoginItems;
import com.roamtech.android.smsleo.util.BulkSMSConstant;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.RootDSE;
import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchScope;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dennis on 10/14/14.
 */
public class AuthenticateTask extends AsyncTask<LDAPServerInstance, Void, String> {
    public static final String TAG = "AuthenticateTask";
    Context context;
    Fragment fragment;
    String userDN;
    public static LDAPConnection connection = null;
    private SharedPreferences prefs;
    String dnUser;
    static final int SIZE_LIMIT = 100;
    static final int TIME_LIMIT_SECONDS = 30;
    LoginItems loginItems;
    String uid;
    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

    private Entry entry = null;

    public AuthenticateTask(Context context,Fragment fragment,String uid){
        this.context = context;
        prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        this.uid = uid;
        this.fragment = fragment;
        loginItems = new LoginItems();
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        ReturnAuthenticationResult ac = (ReturnAuthenticationResult) fragment;
        ac.onStartTask();
    }
    @Override
    protected String doInBackground(LDAPServerInstance... params) {
        String response = null;
        LDAPServerInstance ldapServer = params[0];
        loginItems = authenticate(ldapServer,context);
        if(loginItems.isUser() == true){
            nameValuePairs.add(new BasicNameValuePair(BulkSMSConstant.REQUEST, BulkSMSConstant.CLIENT_ID));
            nameValuePairs.add(new BasicNameValuePair(BulkSMSConstant.ID, loginItems.getClientID()));

            try {
                response = RestClient.makeRestRequest(RestClient.GET, BulkSMSConstant.SITE_URL2 , nameValuePairs);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        ReturnAuthenticationResult ac = (ReturnAuthenticationResult) fragment;
        try {
            ac.onReturnResult(result,loginItems);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LoginItems authenticate(LDAPServerInstance ldapServer,final Context context) {
        String filterString;
        try {
            connection = ldapServer.getConnection();
            if (connection != null) {
                RootDSE s = connection.getRootDSE();
                String[] baseDNs = null;
                if (s != null) {
                    baseDNs = s.getNamingContextDNs();
                }
                filterString = "(uid=" + uid + ')';
                Filter filter = null;
                filter = Filter.create(filterString);

                final SearchRequest request = new SearchRequest(BulkSMSConstant.BASEDNS,
                        SearchScope.SUB, filter);
                request.setSizeLimit(SIZE_LIMIT);
                request.setTimeLimitSeconds(TIME_LIMIT_SECONDS);
                SearchResult result;
                result = connection.search(request);
                Log.d(TAG,"Search done "+result);
                final int entryCount = result.getEntryCount();
                if (entryCount == 0)
                {
                    loginItems.setUser(false);
                    loginItems.setClientID(null);
                }
                else if (entryCount == 1)
                {
                    entry = result.getSearchEntries().get(0);
                    if (hasContactAttributes(entry))
                    {
                        String name = entry.getAttributeValue(BulkSMSConstant.ATTR_UID);
                        String roles = entry.getAttributeValue(BulkSMSConstant.ATTR_EMALI_ROLES) ;
                        String clientId = entry.getAttributeValue((BulkSMSConstant.ATTR_CLIENT_ID));
                        Log.d(TAG,"------------"+name+"---------"+roles+"---------------"+clientId);
                        loginItems.setUser(true);
                        loginItems.setClientID(clientId);
                        //  Toast.makeText(this,"display user",Toast.LENGTH_SHORT).show();
                    }else{
                        loginItems.setUser(false);
                        loginItems.setClientID(null);
                    }
                }
                //getUserDetails("",connection);
                //isGroupContainUser(connection,null,"ou=roamtech,dc=roamtech,dc=com");
            }else{
                loginItems.setUser(false);
                loginItems.setClientID(null);

            }

            return loginItems;
        } catch (LDAPException e) {
            loginItems.setUser(false);
            loginItems.setClientID(null);
            Log.e(TAG, "Error authenticating", e);
            return loginItems;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    private boolean hasContactAttributes(final Entry e)
    {
        // The entry must have a full name attribute to be considered a user.
        if (! e.hasAttribute(BulkSMSConstant.ATTR_UID) & !e.hasAttribute(BulkSMSConstant.ATTR_CLIENT_ID))
        {
            return false;
        }


        // The user also needs at least one of an e-mail address or phone number.
        return (e.hasAttribute(BulkSMSConstant.ATTR_PRIMARY_MAIL) ||
                e.hasAttribute(BulkSMSConstant.ATTR_GIVEN_NAME) ||
                e.hasAttribute(BulkSMSConstant.ATTR_EMALI_ROLES) ||
                e.hasAttribute(BulkSMSConstant.ATTR_SN_NAME) ||
                e.hasAttribute(BulkSMSConstant.ATTR_CN_NAME));
    }
}
