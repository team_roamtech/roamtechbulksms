package com.roamtech.android.smsleo.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by dennis on 6/10/15.
 */
public class CommonUtils {

    public static final String DIALOG = "dialog";
    public static final String SIGNOUT = "sign_out";
    public static final String RETURN_TAG = "return_tag";

    public static void addFragment(Fragment fragment, boolean addToBackStack, int transition,int layoutResourceID, FragmentManager fm, String tag){
        //Whatever
        boolean fragmentPopped = fm.popBackStackImmediate(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (!fragmentPopped && fm.findFragmentByTag(tag) == null) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(layoutResourceID, fragment);
            ft.setTransition(transition);
            if (addToBackStack)
                ft.addToBackStack(tag);
            ft.commit();
        }
    }

    public static FragmentTransaction addDialogFragment(FragmentManager fm,String tag){
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag(tag);
        if (prev != null) {
            ft.remove(prev);
        }
        //ft.addToBackStack(null);
        return ft;
    }

}
