package com.roamtech.android.smsleo.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.adapters.SpinnerSenderAdapter;
import com.roamtech.android.smsleo.async_tasks.SendScheduledMessagesTask;
import com.roamtech.android.smsleo.http.NetworkConnectionStatus;
import com.roamtech.android.smsleo.http.RestClient;
import com.roamtech.android.smsleo.http.RestClient2;
import com.roamtech.android.smsleo.interfaces.ReturnResult;
import com.roamtech.android.smsleo.util.AsyncLoader;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

import org.apache.http.NameValuePair;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuickSMSFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<HashMap<String, String>>>,ReturnResult,SwipeRefreshLayout.OnRefreshListener{
    public static final String TAG = "QuickSMSFragment";
    public static final int LOADER = 1000;
    AppCompatActivity mActivity;
    EditText txtQuickmessage, txtRecipient;
    Spinner spnSender;
    Button btnsendnow, btnclear;
    SharedPreferences app_preference;
    SharedPreferences.Editor editor;
    ProgressDialog progressDialog;
    HashMap<String, String> valuePairs = new HashMap<String, String>();
    String strMessage,strClientID,strRecipient,strSender;
    public static String messageResult;
    public ArrayList<HashMap<String, String>> senders_list;
    SpinnerSenderAdapter spinnerSenderAdapter;
    boolean adapterAdded = false;
    double balance;
    SwipeRefreshLayout mSwipeRefreshLayout;

    public QuickSMSFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BulkSMSConstant.signedOut(mActivity);
        app_preference = PreferenceManager.getDefaultSharedPreferences(mActivity);
        editor = app_preference.edit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_qucksms, container, false);
        txtQuickmessage = (EditText) rootView.findViewById(R.id.txtQuickmessage);
        txtRecipient = (EditText) rootView.findViewById(R.id.txtRecipient);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh_container);
        mSwipeRefreshLayout.setColorSchemeColors(R.color.blue_dark_theme,R.color.blueback,R.color.mycolor);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        spnSender = (Spinner) rootView.findViewById(R.id.spinnersender);
        spnSender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    strSender = senders_list.get(position).get(BulkSMSConstant.SENDER);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }

        });

        if(app_preference.contains(BulkSMSConstant.SENDERS_LIST)){
            String sendersList = app_preference.getString(BulkSMSConstant.SENDERS_LIST,null);
            if(!sendersList.equals(null)){
                senders_list = AddContactGroupFragment.parseSendersJson(sendersList);
            }
        }
        if(senders_list != null) {
            spinnerSenderAdapter = new SpinnerSenderAdapter(mActivity, 0, senders_list);
            spnSender.setAdapter(spinnerSenderAdapter);
        }

        btnsendnow = (Button) rootView.findViewById(R.id.btnsendnow);
        btnsendnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (NetworkConnectionStatus.isOnline(mActivity)) {
                    if ((!txtQuickmessage.getText().toString().trim().equals("")) && (!txtRecipient.getText().toString().trim().equals(""))) {
                        new SendScheduledMessagesTask(mActivity, QuickSMSFragment.this, BulkSMSConstant.FLAG_UPDATE_CUSTOMER_CREDIT).execute(prepareValuePairs2());
                    } else {
                        Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_check_number_message), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_no_connection), Toast.LENGTH_LONG).show();
                }
            }
        });

        btnclear = (Button) rootView.findViewById(R.id.btnclear);
        btnclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                txtQuickmessage.setText("");
                txtRecipient.setText("");
            }
        });
        return rootView;
    }

    public void onResume(){
        super.onResume();
        if(NetworkConnectionStatus.isOnline(mActivity)) {
            mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this).forceLoad();
        }else{
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_connection),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRefresh() {
        if(NetworkConnectionStatus.isOnline(mActivity)) {
            mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
        }else{
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_connection),Toast.LENGTH_LONG).show();
            mSwipeRefreshLayout.setRefreshing(false);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, men
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onStartTask() {

    }

    @Override
    public void onReturnResult(Object result) {
        String strResult = (String) result;
        processResult(strResult);
    }

    public void processResult(String result){
        Log.d(TAG, result);
        JSONObject jsonMessage = null;
        if(result != null) {
            jsonMessage = BulkSMSConstant.parseJson(result);
            if(jsonMessage != null){
                processJSONResult(jsonMessage);
            }else{
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_error_retrieving), Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_error_retrieving), Toast.LENGTH_LONG).show();
        }
        progressDialog.dismiss();
    }

    public void processJSONResult(JSONObject response){
        JSONObject jsonResult = null;
        try {
             if(response.has(BulkSMSConstant.QUICKSMS) & !response.isNull(BulkSMSConstant.QUICKSMS)) {
                jsonResult = response.getJSONObject(BulkSMSConstant.QUICKSMS);
                if(jsonResult.has(BulkSMSConstant.STATUS) & !jsonResult.isNull(BulkSMSConstant.STATUS)) {
                    messageResult = jsonResult.getString(BulkSMSConstant.STATUS);
                    Toast.makeText(mActivity, messageResult, Toast.LENGTH_LONG).show();
                    if(messageResult.equals(BulkSMSConstant.SUCCESS)){

                    }
                }
            }else if(response.has(BulkSMSConstant.CREDIT) & !response.isNull(BulkSMSConstant.CREDIT)){
                 balance =  response.getDouble(BulkSMSConstant.CREDIT);
                 senders_list = null;
                 if (NetworkConnectionStatus.isOnline(mActivity)) {
                     new SendScheduledMessagesTask(mActivity, QuickSMSFragment.this, BulkSMSConstant.FLAG_QUICK_SMS).execute(prepareValuePairs());
                 } else {
                     Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_no_connection), Toast.LENGTH_LONG).show();
                 }
             } else if(response.has(BulkSMSConstant.MESSAGE) & !response.isNull(BulkSMSConstant.MESSAGE)) {
                messageResult =  response.getString(BulkSMSConstant.MESSAGE);
                Toast.makeText(mActivity,messageResult,Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public HashMap<String, String> prepareValuePairs(){
        strMessage = txtQuickmessage.getText().toString();
        strRecipient = txtRecipient.getText().toString();
        strClientID = app_preference.getString(mActivity.getResources().getString(R.string.bundle_customer_account), null);
        valuePairs.put(BulkSMSConstant.TEXT,strMessage);
        valuePairs.put(BulkSMSConstant.CLIENT_ID,strClientID);
        Log.d(TAG,strClientID);
        valuePairs.put(BulkSMSConstant.SENDER,strSender);
        valuePairs.put(BulkSMSConstant.PHONE,strRecipient);
        valuePairs.put(BulkSMSConstant.REQUEST,BulkSMSConstant.QUICKSMS);
        return valuePairs;
    }

    public HashMap<String, String> prepareValuePairs2(){
        strClientID = app_preference.getString(mActivity.getResources().getString(R.string.bundle_customer_account),null);
        valuePairs.put(BulkSMSConstant.CLIENT_ID,strClientID);
        valuePairs.put(BulkSMSConstant.DEDUCT,"1");
        valuePairs.put(BulkSMSConstant.REQUEST,BulkSMSConstant.CREDIT);
        return valuePairs;
    }

    @Override
    public Loader<ArrayList<HashMap<String, String>>> onCreateLoader(int id, Bundle args) {
        if(!app_preference.contains(BulkSMSConstant.SENDERS_LIST)) {
            progressDialog = new ProgressDialog(mActivity);
            progressDialog.setMessage("Loading ...");
            progressDialog.show();
        }
        return new AddContactGroupFragment.GetSenders(mActivity, app_preference.getString(mActivity.getResources().getString(R.string.bundle_customer_account), ""),app_preference);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<HashMap<String, String>>> loader, ArrayList<HashMap<String, String>> data) {
        senders_list = data;
        if(senders_list != null) {
            spinnerSenderAdapter = new SpinnerSenderAdapter(mActivity, 0, senders_list);
            spnSender.setAdapter(spinnerSenderAdapter);
        }else if(messageResult != null && senders_list == null){
            Toast.makeText(mActivity,messageResult,Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_error_retrieving),Toast.LENGTH_LONG).show();
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<HashMap<String, String>>> loader) {

    }
}
