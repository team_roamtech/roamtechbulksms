package com.roamtech.android.smsleo.interfaces;

import android.os.Bundle;
import android.view.View;

/**
 * Created by dennis on 4/7/16.
 */
public interface LaunchResults {
    public void launchResults(int position, Bundle bundle, int view,View itemView);
}
