package com.roamtech.android.smsleo;


import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.fragments.SignInStateFragment;
import com.roamtech.android.smsleo.interfaces.GoToPager;
import com.roamtech.android.smsleo.util.CommonUtils;

public class LoginActivity extends AppCompatActivity implements GoToPager {
    public static final String TAG = "SignInActivity";
    CommonUtils commonUtils;
    SignInStateFragment signInStateFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        signInStateFragment = new SignInStateFragment();
        commonUtils = new CommonUtils();
        commonUtils.addFragment(signInStateFragment, false, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) signInStateFragment).getClass().getName());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void moveToPager(int fragmentPosition, int position) {
        signInStateFragment.addScrollToFragment(position);
    }

}