package com.roamtech.android.smsleo.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.interfaces.LaunchResults;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ManagecontactitemsListLazyAdapter extends RecyclerView.Adapter<ManagecontactitemsListLazyAdapter.PhoneNumbersHolder> {
    public static final String TAG = "ManagecontactitemsListLazyAdapter";
    public static final String LONG_CLICK = "long_click";
    private ArrayList<HashMap<String, String>> data;
    private Context ctx;
    Fragment fragment;
    
    public ManagecontactitemsListLazyAdapter(Context ctx, ArrayList<HashMap<String, String>> d,Fragment fragment) {
        this.ctx = ctx;
        this.fragment = fragment;
        data=d;
    }


    @Override
    public PhoneNumbersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_manage_contactitems, parent, false);
        PhoneNumbersHolder pmh = new PhoneNumbersHolder(v,fragment);
        return pmh;
    }

    @Override
    public void onBindViewHolder(PhoneNumbersHolder holder, int position) {
        HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);
        holder.txtPhoneNo.setText(song.get(BulkSMSConstant.MSISDN));
        holder.txtNetwork.setText(song.get(BulkSMSConstant.CLIENT_ID));
        holder.txtAdded.setText(song.get(BulkSMSConstant.SUBSCRIPTION_DATE));
    }



    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class PhoneNumbersHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener{
        Fragment fragment;
        TextView txtPhoneNo,txtNetwork,txtAdded;
        LinearLayout lytTitle;
        LaunchResults mCallBack;

        public PhoneNumbersHolder(View itemView,Fragment fragment) {
            super(itemView);
            this.fragment = fragment;
            try {
                mCallBack = (LaunchResults) fragment;
            } catch (ClassCastException e) {
                throw new ClassCastException(fragment.toString()
                        + " must implement OnHeadlineSelectedListener");
            }

            txtPhoneNo = (TextView) itemView.findViewById(R.id.phone_no);
            txtNetwork = (TextView) itemView.findViewById(R.id.network);
            txtAdded = (TextView) itemView.findViewById(R.id.added);
            lytTitle = (LinearLayout) itemView.findViewById(R.id.lyt_title);
            lytTitle.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View v) {
            switch(v.getId()){
                case R.id.lyt_title:
                    Bundle args = new Bundle();
                    args.putBoolean(LONG_CLICK,true);
                    mCallBack.launchResults(getAdapterPosition(),args,R.id.lyt_title,v);
                    return true;
                default:

                    return false;
            }
        }
    }
}