package com.roamtech.android.smsleo.interfaces;

/**
 * Created by dennis on 2/26/15.
 */
public interface GoToPager {
    public void moveToPager(int fragmentPosition, int position);
}
