package com.roamtech.android.smsleo.interfaces;

/**
 * Created by dennis on 5/25/15.
 */
public interface CheckConfirm {
    public void confirmYes(String tag);
    public void confirmNo(String tag);
}
