package com.roamtech.android.smsleo.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.adapters.BillingHistoryAdapter;
import com.roamtech.android.smsleo.http.NetworkConnectionStatus;
import com.roamtech.android.smsleo.http.RestClient;
import com.roamtech.android.smsleo.http.RestClient2;
import com.roamtech.android.smsleo.interfaces.LaunchResults;
import com.roamtech.android.smsleo.models.BillingItems;
import com.roamtech.android.smsleo.util.AsyncLoader;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class BillingHistoryFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<BillingItems>>,SwipeRefreshLayout.OnRefreshListener,
        LaunchResults {
    public static final String TAG = "BillingHistoryFragment";
    AppCompatActivity mActivity;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ProgressBar progressBar;
    public static final int LOADER = 1000;
    RecyclerView billingsRecycler;
    public ArrayList<BillingItems> history_list;
    SharedPreferences app_preference;
    SharedPreferences.Editor editor;
    public static String messageResult;
    ProgressBar mProgresBar;
    BillingHistoryAdapter adapter;

    public BillingHistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BulkSMSConstant.signedOut(mActivity);
        app_preference = PreferenceManager.getDefaultSharedPreferences(mActivity);
        editor = app_preference.edit();
        history_list = new ArrayList<BillingItems>();
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.activity_sentmessages, container, false);
        mProgresBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh_container);
        mSwipeRefreshLayout.setColorSchemeColors(R.color.blue_dark_theme, R.color.blueback, R.color.mycolor);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        billingsRecycler = (RecyclerView) rootView.findViewById(R.id.lst_sent);
        LinearLayoutManager llm = new LinearLayoutManager(mActivity);
        billingsRecycler.setLayoutManager(llm);

        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.d(TAG, "substitute");
        mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);

    }

    @Override
    public void launchResults(int position, Bundle bundle, int view, View itemView) {
        switch(view) {
            case R.id.img_down:
                history_list.get(position).setIsExpanded(bundle.getBoolean(BillingHistoryAdapter.ISEXPANDED, false));
                adapter.notifyDataSetChanged();
                break;
            default:
                break;
        }
    }

    public static class GetBillingHistory extends AsyncLoader<ArrayList<BillingItems>> {
        Context ctx;
        public ArrayList<BillingItems> history_list = new ArrayList<BillingItems>();
        String app_preference;
        HashMap<String,String> hashMap = new HashMap<String,String>();

        public GetBillingHistory(Context context,String app_preference) {
            super(context);
            // TODO Auto-generated constructor stub
            this.ctx = context;
            this.app_preference = app_preference;
        }

        @Override
        public ArrayList<BillingItems> loadInBackground() {
            Log.d(TAG, "  loader launched ");
            String response = null;
            JSONObject jsonParams = new JSONObject();
            try {
                hashMap.put(BulkSMSConstant.REQUEST, BulkSMSConstant.BILLING);
                hashMap.put(BulkSMSConstant.CLIENT_ID, app_preference);
                response = RestClient2.makeRestRequest(RestClient2.GET, BulkSMSConstant.SITE_URL2, hashMap);
                Log.d(TAG, response);
                return parseJson(response);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public ArrayList<BillingItems> parseJson(String response){
            JSONObject json = null;
            JSONObject jsonResult = null;
            String message = null;
            try {
                json = new JSONObject(response);
                if(json.has(BulkSMSConstant.RESULT) & !json.isNull(BulkSMSConstant.RESULT)) {
                    jsonResult = json.getJSONObject(BulkSMSConstant.RESULT);
                    if (jsonResult.has(BulkSMSConstant.BILLING) & !jsonResult.isNull(BulkSMSConstant.BILLING)) {
                        JSONArray jsonGroups = new JSONArray(jsonResult.getString(BulkSMSConstant.BILLING));
                        JSONObject c;
                        for (int i = 0; i < jsonGroups.length(); i++) {
                            c = jsonGroups.getJSONObject(i);
                            BillingItems map = new BillingItems();
                            map.setAmount(c.getString(BulkSMSConstant.AMOUNT));
                            map.setBalance(c.getString(BulkSMSConstant.BALANCE));
                            map.setCredit(c.getString(BulkSMSConstant.CREDIT));
                            map.setDebit(c.getString(BulkSMSConstant.DEBIT));
                            map.setCurrency(c.getString(BulkSMSConstant.CURRENCY));
                            map.setTransactionDate(c.getString(BulkSMSConstant.TRANSACTION_DATE_TIME));
                            map.setTransactionType(c.getString(BulkSMSConstant.TRANSACTION_TYPE));
                            history_list.add(map);
                        }
                    }else if(jsonResult.has(BulkSMSConstant.MESSAGE) & !jsonResult.isNull(BulkSMSConstant.MESSAGE)){
                        message =  jsonResult.getString(BulkSMSConstant.MESSAGE);
                        messageResult = message;
                        history_list = null;
                    }else{
                        history_list = null;
                    }
                }else if(json.has(BulkSMSConstant.ERROR) & !json.isNull(BulkSMSConstant.ERROR)){
                    jsonResult = json.getJSONObject(BulkSMSConstant.ERROR);
                    if (jsonResult.has(BulkSMSConstant.MESSAGE) & !jsonResult.isNull(BulkSMSConstant.MESSAGE)) {
                        history_list = null;
                    }
                }else{
                    history_list = null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return history_list;
        }

    }



    @Override
    public Loader<ArrayList<BillingItems>> onCreateLoader(int id, Bundle args) {
        return new GetBillingHistory(mActivity,app_preference.getString(mActivity.getResources().getString(R.string.bundle_customer_account), ""));
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<BillingItems>> loader, ArrayList<BillingItems> data) {
        history_list = data;
        if(history_list != null) {
            adapter =  new BillingHistoryAdapter(mActivity, history_list,this);
            billingsRecycler.setAdapter(adapter);
        }else if(history_list == null & messageResult != null){
            Toast.makeText(mActivity, messageResult, Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(mActivity,messageResult,Toast.LENGTH_LONG).show();
        }
        mProgresBar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onLoaderReset(Loader<ArrayList<BillingItems>> loader) {

    }

    @Override
    public void onRefresh() {
        if(NetworkConnectionStatus.isOnline(mActivity)) {
            mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
        }else{
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_connection),Toast.LENGTH_LONG).show();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }
}
