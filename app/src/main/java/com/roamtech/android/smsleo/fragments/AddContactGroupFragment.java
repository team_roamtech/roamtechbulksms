package com.roamtech.android.smsleo.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.adapters.SpinnerSenderAdapter;
import com.roamtech.android.smsleo.async_tasks.SendScheduledMessagesTask;
import com.roamtech.android.smsleo.http.NetworkConnectionStatus;
import com.roamtech.android.smsleo.http.RestClient2;
import com.roamtech.android.smsleo.interfaces.ReturnResult;
import com.roamtech.android.smsleo.util.AsyncLoader;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AddContactGroupFragment extends Fragment implements View.OnClickListener,ReturnResult,LoaderManager.LoaderCallbacks<ArrayList<HashMap<String, String>>>,SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = "AddContactGroupFragment";
    EditText editGroupName;
    String strGroupName,strClientID;
    Button btnAdd,btnDone;
    SharedPreferences app_preference;
    SharedPreferences.Editor editor;
    ProgressDialog progressDialog;
    AppCompatActivity mActivity;
    SpinnerSenderAdapter spinnerSenderAdapter;
    HashMap<String, String> valuePairs = new HashMap<String, String>();
    public ArrayList<HashMap<String, String>> senders_list;
    public static final int LOADER1 = 1001;
    Spinner spnSender;
    String strSender;
    public static String messageResult;
    SwipeRefreshLayout mSwipeRefreshLayout;

    public AddContactGroupFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app_preference = PreferenceManager.getDefaultSharedPreferences(mActivity);
        editor = app_preference.edit();
        strClientID = app_preference.getString(mActivity.getResources().getString(R.string.bundle_customer_account), null);
        senders_list = new ArrayList<HashMap<String, String>>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_contact_group, container, false);
        editGroupName = (EditText) rootView.findViewById(R.id.edit_group_name);
        editGroupName.setOnClickListener(this);
        btnAdd = (Button) rootView.findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);
        btnDone = (Button) rootView.findViewById(R.id.btn_done);
        btnDone.setOnClickListener(this);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh_container);
        mSwipeRefreshLayout.setColorSchemeColors(R.color.blue_dark_theme, R.color.blueback, R.color.mycolor);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        spnSender = (Spinner) rootView.findViewById(R.id.spinnersender);
        spnSender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strSender = senders_list.get(position).get(BulkSMSConstant.SENDER);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }

        });
        if(app_preference.contains(BulkSMSConstant.SENDERS_LIST)){
            String sendersList = app_preference.getString(BulkSMSConstant.SENDERS_LIST,null);
            if(!sendersList.equals(null)){
                senders_list = parseSendersJson(sendersList);
            }
        }
        if(senders_list != null) {
            spinnerSenderAdapter = new SpinnerSenderAdapter(mActivity, 0, senders_list);
            spnSender.setAdapter(spinnerSenderAdapter);
        }

        if(NetworkConnectionStatus.isOnline(mActivity)) {
            mActivity.getSupportLoaderManager().restartLoader(LOADER1, null, this).forceLoad();
        }else{
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_no_connection), Toast.LENGTH_LONG).show();
        }


        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void fillData(){
        if(editGroupName.getText().length() < 1){
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_enter_details), Toast.LENGTH_LONG).show();
        }else{
            strGroupName = editGroupName.getText().toString();
            if(NetworkConnectionStatus.isOnline(mActivity)) {
                new SendScheduledMessagesTask(mActivity, AddContactGroupFragment.this, BulkSMSConstant.FLAG_ADD_GROUP).execute(prepareValuePairs());
            }else{
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_no_connection), Toast.LENGTH_LONG).show();
            }
        }
    }

    public HashMap<String, String> prepareValuePairs() {

        valuePairs.put(BulkSMSConstant.CLIENT_ID, strClientID);
        valuePairs.put(BulkSMSConstant.GROUPNAME, strGroupName);
        valuePairs.put(BulkSMSConstant.REQUEST, BulkSMSConstant.GROUP);
        return valuePairs;

    }
        @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_add:
                fillData();
                break;
            case R.id.btn_done:
                mActivity.getSupportFragmentManager().popBackStackImmediate();
                break;
        }
    }

    @Override
    public void onRefresh() {
        if(NetworkConnectionStatus.isOnline(mActivity)) {
            mActivity.getSupportLoaderManager().restartLoader(LOADER1, null, this);
        }else{
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_connection),Toast.LENGTH_LONG).show();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public static class GetSenders extends AsyncLoader<ArrayList<HashMap<String, String>>> {
        Context ctx;
        public ArrayList<HashMap<String, String>> senders_list = new ArrayList<HashMap<String, String>>();
        String app_preference;
        HashMap<String,String> hashMap = new HashMap<String,String>();
        SharedPreferences prefs;

        public GetSenders(Context context, String app_preference,SharedPreferences prefs) {
            super(context);
            // TODO Auto-generated constructor stub
            this.ctx = context;
            this.app_preference = app_preference;
            this.prefs = prefs;
        }

        @Override
        public ArrayList<HashMap<String, String>> loadInBackground() {
            Log.d(TAG, "  loader launched ");
            String response = null;
            JSONObject jsonMessage = null;
            try {
                hashMap.put(BulkSMSConstant.REQUEST, BulkSMSConstant.SENDER);
                hashMap.put(BulkSMSConstant.CLIENT_ID, app_preference);

                Log.d(TAG, app_preference);
                response = RestClient2.makeRestRequest(RestClient2.GET, BulkSMSConstant.SITE_URL2, hashMap);
                Log.d(TAG, "--------------- "+response);
                if(response != null) {
                    jsonMessage = BulkSMSConstant.parseJson(response);
                    if(jsonMessage != null){
                        senders_list = parseSendersJson(jsonMessage.toString());
                        if(senders_list != null){
                            if(prefs.contains(BulkSMSConstant.SENDERS_LIST)){
                                prefs.edit().remove(BulkSMSConstant.SENDERS_LIST).commit();
                            }
                            prefs.edit().putString(BulkSMSConstant.SENDERS_LIST, jsonMessage.toString()).commit();
                        }
                    }else{
                        senders_list = null;
                    }
                }else{
                    senders_list = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            return senders_list;
        }
    }

    public static ArrayList<HashMap<String, String>> parseSendersJson(String strResponse) {
        JSONObject response =  null;
        ArrayList<HashMap<String, String>> senders_list = new ArrayList<HashMap<String, String>>();
        String message = null;
        JSONObject jObject = null;
        try {
            response = new JSONObject(strResponse);
            if (response.has(BulkSMSConstant.SENDER) & !response.isNull(BulkSMSConstant.SENDER)) {
                jObject = response.getJSONObject(BulkSMSConstant.SENDER);
                if(jObject.has(BulkSMSConstant.ID) & !jObject.isNull(BulkSMSConstant.ID)) {
                    JSONArray jsonGroups = jObject.getJSONArray(BulkSMSConstant.ID);
                    for (int i = 0; i < jsonGroups.length(); i++) {
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(BulkSMSConstant.SENDER, jsonGroups.get(i).toString());
                        senders_list.add(map);
                    }
                }
            }
            else if(response.has(BulkSMSConstant.MESSAGE) & !response.isNull(BulkSMSConstant.MESSAGE)){
                message =  response.getString(BulkSMSConstant.MESSAGE);
                messageResult = message;
                senders_list = null;
            }else {
                senders_list = null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            senders_list = null;
        }
        return senders_list;
    }


    @Override
    public void onStartTask() {
        progressDialog = new ProgressDialog(mActivity);
        progressDialog.setMessage(mActivity.getResources().getString(R.string.dialog_adding));
        progressDialog.show();
    }

    @Override
    public void onReturnResult(Object result) {
        String strResult = (String) result;
        JSONObject message = null;
        String strMessage = null;
        if(strResult != null){
            message = BulkSMSConstant.parseJson(strResult);
            if(message != null) {
                parseJson(message);
            }else {
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_advisory_group_error), Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_advisory_group_error), Toast.LENGTH_LONG).show();
        }
        Log.d(TAG, "------------ "+strResult);
        progressDialog.dismiss();

    }

    public void parseJson(JSONObject response) {
        JSONObject jsonResult = null;
        String message = null;
        try {
            if (response.has(BulkSMSConstant.GROUP) & !response.isNull(BulkSMSConstant.GROUP)) {
                jsonResult = response.getJSONObject(BulkSMSConstant.GROUP);
                if(jsonResult.has(BulkSMSConstant.INFO) & !jsonResult.isNull(BulkSMSConstant.INFO)) {
                    message = jsonResult.getString(BulkSMSConstant.INFO);
                    Toast.makeText(mActivity, message, Toast.LENGTH_LONG).show();
                    editGroupName.setText("");
                }else{
                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_advisory_group_error), Toast.LENGTH_LONG).show();
                    message = null;
                }
            }else if(response.has(BulkSMSConstant.MESSAGE) & !response.isNull(BulkSMSConstant.MESSAGE)){
                jsonResult = response.getJSONObject(BulkSMSConstant.MESSAGE);
                if(jsonResult.has(BulkSMSConstant.MESSAGE) & !jsonResult.isNull(BulkSMSConstant.MESSAGE)) {
                    message = jsonResult.getString(BulkSMSConstant.MESSAGE);
                    Toast.makeText(mActivity, message, Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_advisory_group_error), Toast.LENGTH_LONG).show();
                    message = null;
                }
            }else {
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_advisory_group_error), Toast.LENGTH_LONG).show();
                message = null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Loader<ArrayList<HashMap<String, String>>> onCreateLoader(int id, Bundle args) {
        if(id == LOADER1){
            if(!app_preference.contains(BulkSMSConstant.SENDERS_LIST)) {
                progressDialog = new ProgressDialog(mActivity);
                progressDialog.setMessage("Loading ...");
                progressDialog.show();
            }
            return new GetSenders(mActivity, app_preference.getString(mActivity.getResources().getString(R.string.bundle_customer_account), ""),app_preference);
        }else{
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<HashMap<String, String>>> loader, ArrayList<HashMap<String, String>> data) {
        if(loader.getId() == LOADER1){
            senders_list = data;
            if(senders_list != null) {
                spinnerSenderAdapter = new SpinnerSenderAdapter(mActivity, 0, senders_list);
                spnSender.setAdapter(spinnerSenderAdapter);
            }else if(messageResult != null && senders_list == null){
                Toast.makeText(mActivity,messageResult,Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_error_retrieving),Toast.LENGTH_LONG).show();
            }
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<HashMap<String, String>>> loader) {

    }
}
