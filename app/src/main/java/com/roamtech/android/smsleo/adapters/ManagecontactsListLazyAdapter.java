package com.roamtech.android.smsleo.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.interfaces.LaunchResults;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ManagecontactsListLazyAdapter extends RecyclerView.Adapter<ManagecontactsListLazyAdapter.ContactsHolder> {
    public static final String TAG = "ManagecontactsListLazyAdapter";
    public static final String LONG_CLICK = "long_click";
    private Activity activity;
    private ArrayList<HashMap<String, String>> groups_list;
    private Context ctx;
    Fragment fragment;


    public ManagecontactsListLazyAdapter(Context ctx, ArrayList<HashMap<String, String>> d,Fragment fragment) {
        this.ctx = ctx;
        this.fragment = fragment;
        groups_list=d;
    }


    @Override
    public ContactsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_manage_contacts, parent, false);
        ContactsHolder cmh = new ContactsHolder(v,ctx,fragment);
        return cmh;
    }

    @Override
    public void onBindViewHolder(ContactsHolder holder, int position) {
        holder.txtGroupName.setText(groups_list.get(position).get(BulkSMSConstant.SERVICE_NAME));
    }

    @Override
    public int getItemCount() {
        return groups_list.size();
    }


    public static class ContactsHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener{
        Fragment fragment;
        Context ctx;
        TextView txtGroupName,txtCreated;
        LinearLayout lytTitle;
        LaunchResults mCallBack;

        public ContactsHolder(View itemView,Context ctx,Fragment fragment) {
            super(itemView);
            this.ctx = ctx;
            this.fragment = fragment;
            try {
                mCallBack = (LaunchResults) fragment;
            } catch (ClassCastException e) {
                throw new ClassCastException(fragment.toString()
                        + " must implement OnHeadlineSelectedListener");
            }
            txtGroupName = (TextView) itemView.findViewById(R.id.group_name);
            txtCreated = (TextView) itemView.findViewById(R.id.created);
            lytTitle = (LinearLayout) itemView.findViewById(R.id.lyt_title);
            lytTitle.setOnClickListener(this);
            lytTitle.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch(v.getId()) {
                case R.id.lyt_title:
                    mCallBack.launchResults(getAdapterPosition(),null,R.id.lyt_title,v);
                    break;
                default:

                    break;

            }
        }

        @Override
        public boolean onLongClick(View v) {
            switch(v.getId()){
                case R.id.lyt_title:
                    Bundle args = new Bundle();
                    args.putBoolean(LONG_CLICK,true);
                    mCallBack.launchResults(getAdapterPosition(),args,R.id.lyt_title,v);
                    return true;
                default:

                    return false;
            }
        }
    }
}