package com.roamtech.android.smsleo.fragments;

import android.content.Context;
import android.support.v4.app.LoaderManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.adapters.ManagecontactsListLazyAdapter;
import com.roamtech.android.smsleo.dialog_fragments.AddContactDialogFragment;
import com.roamtech.android.smsleo.http.NetworkConnectionStatus;
import com.roamtech.android.smsleo.http.RestClient2;
import com.roamtech.android.smsleo.interfaces.LaunchResults;
import com.roamtech.android.smsleo.util.AsyncLoader;
import com.roamtech.android.smsleo.util.BulkSMSConstant;
import com.roamtech.android.smsleo.util.CommonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import android.support.v4.content.Loader;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManageContactsFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<HashMap<String, String>>>,SwipeRefreshLayout.OnRefreshListener
,LaunchResults,PopupMenu.OnMenuItemClickListener {
    public static final String TAG = "ManageContactsFragment";
    public ArrayList<HashMap<String, String>> contacts_list;
    SharedPreferences app_preference;
    SharedPreferences.Editor editor;
    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView contactsRecycler;
    ManagecontactsListLazyAdapter adapter;
    AppCompatActivity mActivity;
    ManageContactsItemsListFragment manageContacts;
    public static final int LOADER = 1000;
    AddContactDialogFragment newDialogFragment1;
    ProgressBar progressBar;
    public static String messageResult;
    int adapterPosition;

    public ManageContactsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity)context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BulkSMSConstant.signedOut(mActivity);
        app_preference = PreferenceManager.getDefaultSharedPreferences(mActivity);
        editor = app_preference.edit();
        manageContacts = new ManageContactsItemsListFragment();
        newDialogFragment1 = new AddContactDialogFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_sentmessages, container, false);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh_container);
        mSwipeRefreshLayout.setColorSchemeColors(R.color.blue_dark_theme,R.color.blueback,R.color.mycolor);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        contactsRecycler = (RecyclerView) rootView.findViewById(R.id.lst_sent);
        LinearLayoutManager llm = new LinearLayoutManager(mActivity);
        contactsRecycler.setLayoutManager(llm);
        contacts_list = new ArrayList<HashMap<String, String>>();

        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.d(TAG,"substitute");
        mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        if(NetworkConnectionStatus.isOnline(mActivity)) {
            mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
        }else{
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_connection),Toast.LENGTH_LONG).show();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void launchResults(int position, Bundle bundle, int view,View itemView) {
        adapterPosition = position;
        if(bundle != null){
            PopupMenu popupMenu = new PopupMenu(mActivity, itemView);
            popupMenu.setOnMenuItemClickListener(this);
            popupMenu.inflate(R.menu.pop_up_contacts_menu);
            popupMenu.show();
        }else {
            if (view == R.id.lyt_title) {
                Bundle args = new Bundle();
                args.putString(BulkSMSConstant.SERVICE_ID, contacts_list.get(adapterPosition).get(BulkSMSConstant.SERVICE_ID));
                args.putString(BulkSMSConstant.SERVICE_NAME, contacts_list.get(adapterPosition).get(BulkSMSConstant.SERVICE_NAME));
                args.putString(BulkSMSConstant.SUBCOUNT, contacts_list.get(adapterPosition).get(BulkSMSConstant.SUBCOUNT));
                args.putString(BulkSMSConstant.CLIENT_ID, app_preference.getString(mActivity.getResources().getString(R.string.bundle_client_id), null));
                manageContacts.setArguments(args);
                CommonUtils.addFragment(manageContacts, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) manageContacts).getClass().getName());
            }
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Bundle args = new Bundle();
        switch (item.getItemId()) {
            case R.id.item_view_contact:
                args.putString(BulkSMSConstant.SERVICE_ID, contacts_list.get(adapterPosition).get(BulkSMSConstant.SERVICE_ID));
                args.putString(BulkSMSConstant.SERVICE_NAME, contacts_list.get(adapterPosition).get(BulkSMSConstant.SERVICE_NAME));
                args.putString(BulkSMSConstant.SUBCOUNT, contacts_list.get(adapterPosition).get(BulkSMSConstant.SUBCOUNT));
                manageContacts.setArguments(args);
                CommonUtils.addFragment(manageContacts, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) manageContacts).getClass().getName());
                return true;
            case R.id.item_add_contact:
                args.putString(BulkSMSConstant.SERVICE_ID, contacts_list.get(adapterPosition).get(BulkSMSConstant.SERVICE_ID));
                args.putString(BulkSMSConstant.SERVICE_NAME, contacts_list.get(adapterPosition).get(BulkSMSConstant.SERVICE_NAME));
                args.putString(BulkSMSConstant.SUBCOUNT, contacts_list.get(adapterPosition).get(BulkSMSConstant.SUBCOUNT));
                newDialogFragment1.setArguments(args);
                newDialogFragment1.setTargetFragment(this, BulkSMSConstant.DIALOG_FRAGMENT_ADD_CONTACTS);
                newDialogFragment1.show(CommonUtils.addDialogFragment(mActivity.getSupportFragmentManager(), "dialog"), "dialog");

                return true;
            default:

                return false;
        }
    }

    public static class GetGroups extends AsyncLoader<ArrayList<HashMap<String, String>>> {
        Context ctx;
        public ArrayList<HashMap<String, String>> groups_list = new ArrayList<HashMap<String, String>>();
        String app_preference;
        HashMap<String,String> hashMap = new HashMap<String,String>();

        public GetGroups(Context context,String app_preference) {
            super(context);
            // TODO Auto-generated constructor stub
            this.ctx = context;
            this.app_preference = app_preference;
        }

        @Override
        public ArrayList<HashMap<String, String>> loadInBackground() {
            Log.d(TAG, "  loader launched ");
            String response = null;
            JSONObject jsonMessage = null;
            try {
                hashMap.put(BulkSMSConstant.CLIENT_ID, app_preference);
                hashMap.put(BulkSMSConstant.REQUEST, BulkSMSConstant.GROUP);
                response = RestClient2.makeRestRequest(RestClient2.GET, BulkSMSConstant.SITE_URL2, hashMap);
                Log.d(TAG, "--------- "+response);
                if(response != null) {
                    jsonMessage = BulkSMSConstant.parseJson(response);
                    if(jsonMessage != null){
                        groups_list = parseJson(jsonMessage);
                    }else{
                        groups_list = null;
                    }
                }else{
                    groups_list = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            return groups_list;
        }

        public ArrayList<HashMap<String, String>> parseJson(JSONObject response){
            String message = null;
            try {
                if (response.has(BulkSMSConstant.GROUPS) & !response.isNull(BulkSMSConstant.GROUPS)) {
                    JSONArray jsonGroups = new JSONArray(response.getString(BulkSMSConstant.GROUPS));
                    JSONObject c;
                    for (int i = 0; i < jsonGroups.length(); i++) {
                        c = jsonGroups.getJSONObject(i);
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(BulkSMSConstant.SUBCOUNT, c.getString(BulkSMSConstant.SUBCOUNT));
                        map.put(BulkSMSConstant.SERVICE_ID, c.getString(BulkSMSConstant.SERVICE_ID));
                        map.put(BulkSMSConstant.SERVICE_NAME, c.getString(BulkSMSConstant.SERVICE_NAME));
                        groups_list.add(map);
                    }
                }else if(response.has(BulkSMSConstant.MESSAGE) & !response.isNull(BulkSMSConstant.MESSAGE)){
                    message =  response.getString(BulkSMSConstant.MESSAGE);
                    messageResult = message;
                    groups_list = null;
                }else {
                    groups_list = null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return groups_list;
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, men
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public Loader<ArrayList<HashMap<String, String>>> onCreateLoader(int i, Bundle bundle) {
        progressBar.setVisibility(View.VISIBLE);
        return new GetGroups(mActivity,app_preference.getString(mActivity.getResources().getString(R.string.bundle_customer_account), ""));
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<HashMap<String, String>>> arrayListLoader, ArrayList<HashMap<String, String>> hashMaps) {
        progressBar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
        contacts_list = hashMaps;
        if(contacts_list != null) {
            adapter =  new ManagecontactsListLazyAdapter(mActivity, contacts_list,this);
            contactsRecycler.setAdapter(adapter);
        }else if(contacts_list == null & messageResult != null){
            Toast.makeText(mActivity, messageResult, Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(mActivity,messageResult,Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<HashMap<String, String>>> arrayListLoader) {

    }
}
