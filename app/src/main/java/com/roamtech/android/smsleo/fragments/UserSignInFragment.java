package com.roamtech.android.smsleo.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.roamtech.android.smsleo.HomeActivity2;
import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.async_tasks.AuthenticateTask;
import com.roamtech.android.smsleo.client.LDAPServerInstance;
import com.roamtech.android.smsleo.http.NetworkConnectionStatus;
import com.roamtech.android.smsleo.interfaces.GoToPager;
import com.roamtech.android.smsleo.interfaces.ReturnAuthenticationResult;
import com.roamtech.android.smsleo.models.LoginItems;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserSignInFragment extends Fragment implements ReturnAuthenticationResult,View.OnClickListener {
    public static final String TAG = "UserSignInFragment";
    EditText username, password, email;
    Button btnLogin;
    String strUsername,strPassword,strEmail,passWord;
    SharedPreferences app_preference;
    SharedPreferences.Editor editor;
    String dnUser= null;
    String mHost= null;
    private int mPort;
    private int mEncryption = 0;
    LDAPServerInstance ldapServer;
    ProgressDialog progressDialog;
    AppCompatActivity mActivity;
    Boolean isVisible;
    TextView txtNoAccount;
    GoToPager mCallBack2;


    public UserSignInFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            mCallBack2 = (GoToPager)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_login, container, false);

        btnLogin = (Button) rootView.findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        username = (EditText) rootView.findViewById(R.id.eTextUsername);
        password = (EditText) rootView.findViewById(R.id.eTextPassword);
        email = (EditText) rootView.findViewById(R.id.eTextEmail);
        txtNoAccount = (TextView) rootView.findViewById(R.id.txt_no_account);
        txtNoAccount.setOnClickListener(this);

        app_preference = PreferenceManager.getDefaultSharedPreferences(mActivity);
        editor = app_preference.edit();

        return rootView;
    }

    //hash the entered password
    private static String getHash(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String result = input;
        if (input != null) {
            MessageDigest md = MessageDigest.getInstance("SHA-256"); // or
            // "SHA-1"
            md.update(input.getBytes());
            BigInteger hash = new BigInteger(1, md.digest());
            result = hash.toString(16);
            while (result.length() < 32) { // 40 for SHA-1
                result = "0" + result;
            }
        }
        return result;
    }

    public void getLDAPServerDetails() {
        Log.i(TAG, "handleLogin");

        strUsername = username.getText().toString();
        strUsername = strUsername.replace(" ", "");
        String uid="uid="+strUsername;
        String dn = BulkSMSConstant.SERVER_DETAILS;
        dnUser= uid+","+dn;


        try {
            passWord = password.getText().toString();
            passWord = passWord.replace(" ", "");
            String salt = passWord + "{" + strUsername + "}";
            // String salt = mPasswordEdit.getText().toString()+"{" + mUsername + "}";
            strPassword = getHash(salt);
            // password = "831c6debd29c51cc69db44b0aab3c9f0c2be491191bd3173e1e3dc804d3dc786";
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //mPassword = mPasswordEdit.getText().toString();
        mHost = BulkSMSConstant.USER_DIRECTORY;
        try {
            mPort = 389;
        } catch (NumberFormatException nfe) {
            Log.i(TAG, "No port given. Set port to 389");
            mPort = 389;
        }//http://roamtech.com/


        ldapServer = new LDAPServerInstance(mHost, mPort, mEncryption, dnUser, strPassword);


        new AuthenticateTask(mActivity,this,strUsername).execute(ldapServer);
        // Start authenticating...
        //ldapUtilities.attemptAuth(ldapServer, LoginActivity.this);
    }

    @Override
    public void onStartTask() {
        progressDialog = new ProgressDialog(mActivity);
        progressDialog.setMessage("Signing in...");
        progressDialog.show();
        progressDialog.setButton(ProgressDialog.BUTTON_NEGATIVE, getString(R.string.okay), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });


    }

    @Override
    public void onReturnResult(String result,LoginItems loginItems){
        String customerAccount = getCustomerAccount(result);
        if(loginItems != null) {
            if (loginItems.isUser() == true & customerAccount != null) {
                if (mActivity.getSupportFragmentManager() != null) {
                    if (app_preference.contains(getString(R.string.bundle_username))) {
                        Log.d(TAG, "Remove prefs");
                        app_preference.edit().remove(getString(R.string.bundle_username)).commit();
                    }
                    editor.putString(getString(R.string.bundle_username), strUsername);
                    editor.commit();

                    if (app_preference.contains(getString(R.string.bundle_password))) {
                        Log.d(TAG, "Remove prefs");
                        app_preference.edit().remove(getString(R.string.bundle_password)).commit();
                    }
                    editor.putString(getString(R.string.bundle_password), strPassword);
                    editor.commit();


                    if (app_preference.contains(getString(R.string.bundle_client_id))) {
                        Log.d(TAG, "Remove prefs");
                        app_preference.edit().remove(getString(R.string.bundle_client_id)).commit();
                    }
                    editor.putString(getString(R.string.bundle_client_id), loginItems.getClientID());
                    editor.commit();

                    if (app_preference.contains(getString(R.string.bundle_customer_account))) {
                        Log.d(TAG, "Remove prefs");
                        app_preference.edit().remove(getString(R.string.bundle_customer_account)).commit();
                    }

                    editor.putString(getString(R.string.bundle_customer_account), customerAccount);
                    editor.commit();

                    progressDialog.dismiss();

                    Intent i = new Intent(mActivity, HomeActivity2.class);
                    startActivity(i);
                    mActivity.finish();
                }
            } else {
                progressDialog.dismiss();
                Toast.makeText(mActivity, getString(R.string.toast_advisory_invalid_details), Toast.LENGTH_LONG).show();
            }
        }else{
            progressDialog.dismiss();
            Toast.makeText(mActivity, getString(R.string.login_invalid_error), Toast.LENGTH_LONG).show();
        }
    }

    //Parse Json and return customer account

    public String getCustomerAccount(String result){
        String customerAccount = null;
        JSONObject json = null;
        JSONObject json2 = null;
        JSONObject json3 = null;
        if(result != null){
            try {
                json = new JSONObject(result);
                if(json.has(BulkSMSConstant.RESULT) & !json.isNull(BulkSMSConstant.RESULT)) {
                    json2 = json.getJSONObject(BulkSMSConstant.RESULT);
                    if(json2.has(BulkSMSConstant.CLIENT_ID) & !json2.isNull(BulkSMSConstant.CLIENT_ID)) {
                        json3 = json2.getJSONObject(BulkSMSConstant.CLIENT_ID);
                        if (json3.has(BulkSMSConstant.CUSTOMER_ACCOUNT) & !json3.isNull(BulkSMSConstant.CUSTOMER_ACCOUNT)) {
                            customerAccount = json3.getString(BulkSMSConstant.CUSTOMER_ACCOUNT);
                            Log.d(TAG, customerAccount);
                        }
                    }
                }else if(json.has(BulkSMSConstant.ERROR) & !json.isNull(BulkSMSConstant.ERROR)){
                    json2 = json.getJSONObject(BulkSMSConstant.ERROR);
                    if(json2.has(BulkSMSConstant.MESSAGE) & !json2.isNull(BulkSMSConstant.MESSAGE)){
                        customerAccount = null;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return customerAccount;

    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnLogin:
                if(NetworkConnectionStatus.isOnline(mActivity) ){
                    getLDAPServerDetails();
                }else{
                    Toast.makeText(mActivity, getString(R.string.toast_no_connection), Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.txt_no_account:
                mCallBack2.moveToPager(0,1);
                break;
            default:

                break;
        }
    }
}
