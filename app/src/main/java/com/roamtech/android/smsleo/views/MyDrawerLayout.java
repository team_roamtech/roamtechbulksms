package com.roamtech.android.smsleo.views;

import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.KeyEvent;

public class MyDrawerLayout extends DrawerLayout {
	DrawerLayout mdl;
	

	    public MyDrawerLayout(Context context, AttributeSet attrs, int defStyle) {
	        super(context, attrs, defStyle);
	    }

	    public MyDrawerLayout(Context context, AttributeSet attrs) {
	        super(context, attrs);
	    }

	    public MyDrawerLayout(Context context) {
	        super(context);
	    }

	    @Override
	    public boolean onKeyUp(int keyCode, KeyEvent event) {
	        if (keyCode == KeyEvent.KEYCODE_BACK) {
	            return false;
	        }
	        return super.onKeyUp(keyCode, event);
	    }

	    @Override
	    public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if (keyCode == KeyEvent.KEYCODE_BACK) {
	        	
	            return false;
	        }
	        return super.onKeyDown(keyCode, event);
	    }
	    
	    
	    
	    
}
