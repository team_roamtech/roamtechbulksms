package com.roamtech.android.smsleo.adapters;

import android.content.Context;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roamtech.android.smsleo.R;
import com.roamtech.android.smsleo.interfaces.LaunchResults;
import com.roamtech.android.smsleo.models.BillingItems;
import com.roamtech.android.smsleo.util.BulkSMSConstant;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dennis on 7/28/15.
 */
public class BillingHistoryAdapter extends RecyclerView.Adapter<BillingHistoryAdapter.BillingsHolder> {
    public static final String TAG = "BillingHistoryAdapter";
    public static final String ISEXPANDED = "isexpanded";
    Context ctx;
    private ArrayList<BillingItems> data;
    private static LayoutInflater inflater=null;
    Fragment fragment;

    public BillingHistoryAdapter(Context context,ArrayList<BillingItems> data,Fragment fragment){
        this.ctx = context;
        this.fragment = fragment;
        this.data=data;
    }

    @Override
    public BillingsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_billing_history, parent, false);
        BillingsHolder bmh = new BillingsHolder(v,fragment);
        return bmh;
    }

    @Override
    public void onBindViewHolder(BillingsHolder holder, int position) {
        BillingItems billing = data.get(position);
        holder.txtAmount.setText(billing.getAmount());
        holder.txtBalance.setText(billing.getBalance());
        holder.txtCredit.setText(billing.getCredit());
        holder.txtDebit.setText(billing.getDebit());
        holder.txtTransactionDateTime.setText(billing.getTransactionDate());
        holder.txtTransactionType.setText(billing.getTransactionType());
        if(billing.isExpanded == true){
            holder.lytHidden.setVisibility(View.VISIBLE);
            holder.imgDown.setImageResource(R.drawable.collapse_arrow);
        }else if(billing.isExpanded == false){
            holder.lytHidden.setVisibility(View.GONE);
            holder.imgDown.setImageResource(R.drawable.expand_arrow);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class BillingsHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView txtAmount;
        public TextView txtBalance;
        public TextView txtCredit;
        public TextView txtDebit;
        public TextView txtTransactionDateTime;
        public TextView txtTransactionType;
        public LinearLayout lytHidden;
        ImageView imgDown;
        Fragment fragment;
        LaunchResults mCallBack;

        public BillingsHolder(View itemView,Fragment fragment){
            super(itemView);
            this.fragment = fragment;
            try {
                mCallBack = (LaunchResults) fragment;
            } catch (ClassCastException e) {
                throw new ClassCastException(fragment.toString()
                        + " must implement OnHeadlineSelectedListener");
            }
            txtAmount = (TextView) itemView.findViewById(R.id.txt_amount);
            txtBalance = (TextView) itemView.findViewById(R.id.txt_balance);
            txtCredit = (TextView) itemView.findViewById(R.id.txt_credit);
            txtDebit = (TextView) itemView.findViewById(R.id.txt_debit);
            txtTransactionDateTime = (TextView) itemView.findViewById(R.id.txt_transaction_date);
            txtTransactionType = (TextView) itemView.findViewById(R.id.txt_transaction_type);
            imgDown = (ImageView) itemView.findViewById(R.id.img_down);
            lytHidden = (LinearLayout) itemView.findViewById(R.id.lyt_hidden);
            if(lytHidden.getVisibility() == View.VISIBLE) {
                imgDown.setImageResource(R.drawable.collapse_arrow);
            }else{
                imgDown.setImageResource(R.drawable.expand_arrow);
            }
            imgDown.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.img_down:
                    Bundle args = new Bundle();
                    if(lytHidden.getVisibility() == View.GONE) {
                        args.putBoolean(ISEXPANDED,true);
                    }else if(lytHidden.getVisibility() == View.VISIBLE){
                        args.putBoolean(ISEXPANDED,false);
                    }
                    mCallBack.launchResults(getAdapterPosition(), args, R.id.img_down, v);
                    break;
                default:

                    break;

            }
        }
    }


}
